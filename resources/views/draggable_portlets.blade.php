@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" href="{{ asset('css/portlet.css') }}"/>
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('js/custom_js/draggable_portlets.js') }}" type="text/javascript"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    <h1>
        Draggable Portlets
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">
                UI Features
            </a>
        </li>
        <li class="active">
            <a href="{{ url('draggable_portlets') }}">
                Draggable Portlets
            </a>
        </li>
    </ol>
</section>
{{-- Main content --}}
<section class="content">
    <div class="row ui-sortable" id="sortable_portlets">
        <div class="col-md-4 sortable">
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-primary">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-success">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-info">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
        </div>
        <div class="col-md-4 sortable">
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-danger">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-info">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-warning">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
        </div>
        <div class="col-md-4 sortable">
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-warning">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box">
                <div class="portlet-title bg-primary">
                    <div class="caption">
                        <i class="fa fa-fw fa-bars"></i> Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
            {{-- BEGIN Portlet PORTLET--}}
            <div class=" portlet box notsort">
                <div class="portlet-title bg-danger">
                    <div class="caption">
                        <i class="fa fa-fw fa-exclamation"></i> Non Draggable Portlet
                    </div>
                </div>
                <div class="portlet-body">
                    <div>
                        Tom loves Canada. Angela and Tom met. Angela and Tom want to play. Angela and Tom want
                        to jump. Angela and Tom want to yell. Angela and Tom play, jump and yell.
                    </div>
                </div>
            </div>
            {{-- END Portlet PORTLET--}}
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection