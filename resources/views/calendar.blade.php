@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link href="{{ asset('vendors/fullcalendar/css/fullcalendar.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('vendors/fullcalendar/css/fullcalendar.print.css') }}" rel="stylesheet" media='print' type="text/css">
    <link href="{{ asset('vendors/iCheck/css/all.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link href="{{ asset('css/calendar_custom.css') }}" rel="stylesheet" type="text/css"/>
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/fullcalendar/js/fullcalendar.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/iCheck/js/icheck.js') }}"></script>
<script src="{{ asset('js/custom_js/calendar_custom.js') }}" type="text/javascript"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    <h1>Calendar</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li class="active">
            Calendar
        </li>
    </ol>
</section>
{{-- Main content --}}
<section class="content">
    <div class="row">
        <div class="col-md-3">
            <div class="box">
                <div class="box-title">
                    <h3>Draggable Events</h3>
                    <div class="pull-right box-toolbar">
                        <a href="#" class="btn btn-link btn-xs" data-toggle="modal" data-target="#myModal">
                            <i class="fa fa-plus"></i>
                        </a>
                    </div>
                </div>
                <div class="box-body">
                    <div id='external-events'>
                        <div class='external-event palette-warning'>Team Out</div>
                        <div class='external-event palette-primary'>Product Seminar</div>
                        <div class='external-event palette-danger'>Client Meeting</div>
                        <div class='external-event palette-info'>Repeating Event</div>
                        <div class='external-event palette-success'>Anniversary Celebrations</div>
                        <p class="well no-border no-radius">
                            <input type='checkbox' class="custom_icheck" id='drop-remove'/>
                            <label for='drop-remove'>remove after drop</label>
                        </p>
                    </div>
                </div>
                <div class="box-footer">
                    <a href="#" class="btn btn-success btn-block" data-toggle="modal" data-target="#myModal">Create
                        event</a>
                </div>
            </div>
            {{-- /.box --}}
        </div>
        <div class="col-md-9">
            <div class="box">
                <div class="box-body">
                    <div id="calendar"></div>
                </div>
            </div>
            {{-- /.box --}}
        </div>
        {{-- /.col --}}
    </div>
    {{-- Modal --}}
    <div class="modal fade" id="myModal" tabindex="-1" role="dialog" aria-labelledby="myModalLabel"
         aria-hidden="true">
        <div class="modal-dialog">
            <div class="modal-content">
                <div class="modal-header">
                    <button type="button" class="close" data-dismiss="modal" aria-hidden="true">&times;</button>
                    <h4 class="modal-title" id="myModalLabel">
                        <i class="fa fa-plus"></i> Create Event
                    </h4>
                </div>
                <div class="modal-body">
                    <div class="input-group">
                        <input type="text" id="new-event" class="form-control" placeholder="Event">
                        <div class="input-group-btn">
                            <button type="button" id="color-chooser-btn" class="btn btn-info dropdown-toggle"
                                    data-toggle="dropdown">
                                Select
                                <span class="caret"></span>
                            </button>
                            <ul class="dropdown-menu pull-right" id="color-chooser">
                                <li>
                                    <a class="palette-primary" href="#">Primary</a>
                                </li>
                                <li>
                                    <a class="palette-success" href="#">Success</a>
                                </li>
                                <li>
                                    <a class="palette-info" href="#">Info</a>
                                </li>
                                <li>
                                    <a class="palette-warning" href="#">warning</a>
                                </li>
                                <li>
                                    <a class="palette-danger" href="#">Danger</a>
                                </li>
                                <li>
                                    <a class="palette-default" href="#">Default</a>
                                </li>
                            </ul>
                        </div>
                        {{-- /btn-group --}}
                    </div>
                    {{-- /input-group --}}
                </div>
                <div class="modal-footer">
                    <button type="button" class="btn btn-danger pull-right" id="close_calendar_event"
                            data-dismiss="modal">
                        Close
                        <i class="fa fa-times"></i>
                    </button>
                    <button type="button" class="btn btn-success pull-left" id="add-new-event"
                            data-dismiss="modal">
                        <i class="fa fa-plus"></i> Add
                    </button>
                </div>
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection