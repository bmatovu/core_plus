@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" href="{{ asset('vendors/datetime/css/jquery.datetimepicker.css') }}">
    <link href="{{ asset('vendors/airdatepicker/css/datepicker.min.css') }}" rel="stylesheet" type="text/css">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/datetime/js/jquery.datetimepicker.full.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/airdatepicker/js/datepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/airdatepicker/js/datepicker.en.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom_js/advanceddate_pickers.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    {{--section starts--}}
    <h1>
        Advanced Date pickers
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
        <li>
            Advanced Date pickers
        </li>
    </ol>
</section>
{{--section ends--}}
<section class="content">
    {{--main content--}}
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-calendar"></i> DateTime picker
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="box-body">
                        {{-- Date range --}}
                        <div class="form-group">
                            <label for="datetimepicker">
                                Date and Time Picker:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="datetimepicker"/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label for='datetimepicker1'>
                                Date Picker:
                            </label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="datetimepicker1">
                                        <span class="input-group-addon">
                                                <i class="fa fa-fw fa-calendar"></i>
                                            </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="datetimepicker2">
                                Time Picker:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-clock-o"></i>
                                </div>
                                <input id="datetimepicker2" size="30" value="" class="form-control">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label for="datetimepicker_unixtime">
                                Date Time Picker from Unixtime:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input id="datetimepicker_unixtime" class="form-control" value="">
                                <span></span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="datetimepicker7">
                                MinDate and MaxDate:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input class="form-control" id="datetimepicker7" value="">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        <div class="form-group">
                            <label for="datetimepicker8">
                                Invert settings minDate and maxDate:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input class="form-control" id="datetimepicker8">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        <div class="form-group">
                            <div class="row">
                                <div class="col-md-12">
                                    <label for="check_in_date">Check-In, Check-out Date Picker:</label>
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group m-t-10">
                                    <div class="input-group-addon">
                                        <i class="fa fa-fw fa-calendar"></i></div>
                                    <input class="form-control" id="check_in_date" placeholder="Check-In Date">
                                </div>
                            </div>
                            <div class="col-md-6">
                                <div class="input-group m-t-10">
                                    <div class="input-group-addon">
                                        <i class="fa fa-fw fa-calendar"></i></div>
                                    <input class="form-control" id="check_out_date"
                                           placeholder="Check Out Date">
                                </div>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                    </div>
                    {{-- /.form group --}}
                    {{-- time picker --}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-calendar"></i> Air Date Picker
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="box-body">
                        {{-- Date range --}}
                        <div class="form-group">
                            <label for="my-element">
                                Date Picker:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" data-language='en'
                                       id="my-element"/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label for="my-element1">
                                Multiple Date Picker:
                            </label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" data-language='en'
                                       data-multiple-dates="3" data-multiple-dates-separator=", "
                                       id="my-element1"/>
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-calendar"></i>
                                            </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="monthpicker">
                                Month and Year selection:
                            </label>
                            <div class="input-group clockpicker-with-callbacks">
                                <input type="text" class="form-control" data-language='en'
                                       data-min-view="months" data-view="months" data-date-format="MM yyyy"
                                       id="monthpicker"/>
                                        <span class="input-group-addon">
                                            <i class="fa fa-fw fa-calendar-o"></i>
                                            </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="minMaxExample">
                                Minimum and Maximum Dates:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-clock-o"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="minMaxExample">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label for="timepick">
                                Date and Time Picker:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar-o"></i>
                                </div>
                                <input id="timepick" class="form-control pull-right" data-language='en'
                                       data-timepicker="true" data-time-format='hh:ii aa'/>
                            </div>
                        </div>
                        {{-- /.form group --}}
                        {{-- time picker --}}
                        {{-- range of dates --}}
                        <div class="form-group">
                            <label for="dateranges">
                                Range of Dates:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" data-range="true" data-multiple-dates-separator=" - "
                                       data-language="en" class="form-control" id="dateranges"/>
                            </div>
                        </div>
                        {{-- /.form group --}}
                        {{-- Disable days of week --}}
                        <div class="form-group">
                            <label for="disabled-days">
                                Disable Days of Week:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" id="disabled-days"/>
                            </div>
                        </div>
                        {{-- /.end Disable days of week --}}
                        {{-- Modal --}}
                        <button type="button" class="btn btn-primary" data-toggle="modal"
                                data-target="#large_modal">Date Time picker Modal
                        </button>
                        <div id="large_modal" class="modal fade animated " role="dialog">
                            <div class="modal-dialog ">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close"
                                                data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Date and Time Picker</h4>
                                    </div>
                                    <div class="modal-body">
                                        <div class="input-group">
                                            <input data-format="dd/MM/yyyy hh:mm:ss" type="text"
                                                   id="datetimepicker12" class="form-control"/>
                                            <div class="input-group-addon">
                                                <i class="fa fa-fw fa-calendar"></i>
                                            </div>
                                        </div>
                                        <div class="modal-footer">
                                            <button type="button" class="btn" data-dismiss="modal">
                                                Close
                                            </button>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--main content ends--}}
</section>
{{-- /.content --}}

@endsection