@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom_css/circle_sliders.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('vendors/jquery-knob/js/jquery.knob.js') }}"></script>
<script src="{{ asset('js/custom_js/sparkline/jquery.flot.spline.js') }}"></script>
<script src="{{ asset('js/custom_js/circle_sliders.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    {{--section starts--}}
    <h1>
        Circle sliders
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">
                Charts
            </a>
        </li>
        <li class="active">
            Circle sliders
        </li>
    </ol>
</section>
<section class="content">
    {{--main content--}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-circle-o-notch"></i> Circle Dials
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    {{--knob--}}
                    <div class="visible-ie8">
                        <div class="col-md-12">
                            <div class="alert alert-warning alert-dismissable">
                                <button type="button" class="close" data-dismiss="alert"
                                        aria-hidden="true">&times;</button>
                                The Circle Dial plugin is not compatible with Internet Explorer 8 and older.
                            </div>
                        </div>
                    </div>
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">Disabled Display Input</div>
                                    <input class="knob" data-width="120" data-height="120"
                                           data-fgColor="#428BCA" data-displayinput=false value="35">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">Cursor</div>
                                    <input class="knob" data-width="120" data-height="120" data-cursor=true
                                           data-fgColor="#22d69d" data-thickness=.3 value="29">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">
                                        Display previous
                                    </div>
                                    <input class="knob" data-width="120" data-height="120"
                                           data-fgColor="#ffb65f" data-min="-100" data-displayprevious=true
                                           value="44">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">
                                        Angle offset
                                    </div>
                                    <input class="knob" data-angleoffset="90" data-fgcolor="#fb8678"
                                           data-width="120" data-height="120" value="35">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12 col-md-12">
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">
                                        Angle offset arc
                                    </div>
                                    <input class="knob" data-angleoffset="-125" data-anglearc="251"
                                           data-fgcolor="#ffb65f" data-width="120" data-height="120" value="35">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">
                                        5-digit values
                                    </div>
                                    <input class="knob" data-min="-15000" data-fgColor="#fb8678"
                                           data-max="15000" data-width="120" data-height="120" value="-11000">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">Responsive</div>
                                    <input class="knob" data-width="120" data-height="120"
                                           data-fgColor="#22d69d" value="35">
                                </div>
                                <div class="col-md-3 col-sm-3 col-xs-12 text-center m-t-10">
                                    <div class="text-left m-t-10 m-b-10">Readonly</div>
                                    <input class="knob" data-fgColor="#4fc1e9" data-thickness=".4"
                                           data-width="120" data-height="120" readonly value="22">
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-md-12">
                                <div class="demo">
                                    <div class="text-left">Superpose (clock)</div>
                                    <div>
                                        <div class="demo_hours">
                                            <input class="knob hour" data-min="0" data-max="24"
                                                   data-bgColor="#dcdcdc" data-fgColor="#ffb65f"
                                                   data-displayInput=false data-width="240" data-height="240"
                                                   data-thickness=".3" value="15">
                                        </div>
                                        <div class="demo_minutes">
                                            <input class="knob minute" data-min="0" data-max="60"
                                                   data-bgColor="#dcdcdc" data-fgColor="#4fc1e9"
                                                   data-displayInput=false data-width="160" data-height="160"
                                                   data-thickness=".45" value="30">
                                        </div>
                                        <div class="demo_seconds">
                                            <input class="knob second" data-min="0" data-max="60"
                                                   data-bgColor="#dcdcdc" data-fgColor="#22d69d"
                                                   data-displayInput=false data-width="80" data-height="80"
                                                   data-thickness=".3" value="20">
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                {{--knob ends--}}
            </div>
        </div>
    </div>
    {{-- sparkline --}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-bar-chart"></i> Tiny Charts
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="row sparkline_charts">
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny line chart</div>
                                <div class="chart linechart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny bar chart</div>
                                <div class="chart barchart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny stacked bar chart</div>
                                <div class="m-t-10 chart stackedbarchart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny tristate chart</div>
                                <div class="m-t-10 chart tristatechart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny bullet chart</div>
                                <div class="m-t-10 chart bulletchart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny pie chart</div>
                                <div class="m-t-10 chart piechart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny discrete chart</div>
                                <div class="m-t-10 chart discretechart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny boxplot chart</div>
                                <div class="m-t-10 chart boxchart">Loading...</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny composite line chart</div>
                                <div id="compositeline" class="m-t-10">
                                    8,4,0,0,0,0,1,4,4,10,10,10,10,0,0,0,4,6,5,9,10
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny composite bar chart</div>
                                <div id="compositebar" class="m-t-10">4,6,7,7,4,3,2,1,4</div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny line chart with normal range</div>
                                <div id="normalline" class="m-t-10">
                                    8,4,0,0,0,0,1,4,4,10,10,10,10,0,0,0,4,6,5,9,10
                                </div>
                            </div>
                        </div>
                        <div class="col-md-4 m-t-25 text-center">
                            <div>
                                <div>Tiny discrete chart with treshold</div>
                                <div id="discrete2" class="m-t-10">4,6,7,7,4,3,2,1,4</div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- sparkline --}}
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection