@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link type="text/css" href="{{ asset('vendors/imagehover/css/imagehover.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom_css/img_hover.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}

{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
    <section class="content-header">
        <h1>
            Image Hover
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-fw fa-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#"> Gallery</a>
            </li>
            <li class="active">
                Image Hover
            </li>
        </ol>
    </section>
    {{-- Main content --}}
    <section class="content">
        <div class="row">
            <div class="col-lg-12">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw fa-picture-o"></i> Image Hover
                        </h3>
                    </div>
                    <div class="panel-body">
                        <div class="row">
                            <div class="col-md-12">
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-fade">
                                        <img src="{{ asset('img/gallery/full/7.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Fade
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-push-up">
                                        <img src="{{ asset('img/gallery/full/8.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Push-up
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-push-down">
                                        <img src="{{ asset('img/gallery/full/11.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Push-down
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-slide-right">
                                        <img src="{{ asset('img/gallery/full/14.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Slide-right
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-slide-left">
                                        <img src="{{ asset('img/gallery/full/17.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Slide-left
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-reveal-up">
                                        <img src="{{ asset('img/gallery/full/19.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Reveal-up
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-reveal-left">
                                        <img src="{{ asset('img/gallery/full/20.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Reveal-left
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-reveal-down">
                                        <img src="{{ asset('img/gallery/full/24.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Reaveal-down
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-hinge-up">
                                        <img src="{{ asset('img/gallery/full/28.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Hinge-up
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-hinge-right">
                                        <img src="{{ asset('img/gallery/full/29.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Hinge-right
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-flip-horiz">
                                        <img src="{{ asset('img/gallery/full/32.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Flip-horizontal
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-flip-vert">
                                        <img src="{{ asset('img/gallery/full/33.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Flip-vertical
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-flip-diag-1">
                                        <img src="{{ asset('img/gallery/full/30.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Flip-diagonal
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-shutter-out-vert">
                                        <img src="{{ asset('img/gallery/full/14.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Shutter-out-vertical
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-fold-right">
                                        <img src="{{ asset('img/gallery/full/19.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Fold-right
                                        </figcaption>
                                    </figure>
                                </div>
                                <div class="col-md-3 col-sm-6 col-xs-12 text-center">
                                    <figure class="imghvr-zoom-out-down">
                                        <img src="{{ asset('img/gallery/full/17.jpg') }}" class="img-responsive" width="295"
                                             height="185">
                                        <figcaption>
                                            Zoom out down
                                        </figcaption>
                                    </figure>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div id="right">
            <div id="slim2">
                <div class="rightsidebar-right">
                    <div class="rightsidebar-right-content">
                        <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                            <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                        </h5>
                        <ul class="list-unstyled margin-none">
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-primary"></i> Alanis
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-primary"></i> Rolando
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-primary"></i> Marlee
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-warning"></i> Marlee
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-muted"></i> Cielo
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-muted"></i> Charlene
                                </a>
                            </li>
                        </ul>
                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                            <i class="fa fa-fw fa-tasks"></i> Tasks
                        </h5>
                        <ul class="list-unstyled m-t-25">
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <small class="pull-right text-muted">
                                            40% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <small class="pull-right text-muted">
                                            20% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar"
                                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <small class="pull-right text-muted">
                                            60% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <small class="pull-right text-muted">
                                            80% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                            <i class="fa fa-fw fa-group"></i> Recent Activities
                        </h5>
                        <div>
                            <ul class="list-unstyled">
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- /.content --}}

@endsection