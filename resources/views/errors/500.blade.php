@extends ("layouts.base_min")

@section('extra-css')
    @parent
    {{--end of global css--}}
    <link href="{{ asset('css/404.css') }}" rel="stylesheet" type="text/css"/>
    {{--end page level css--}}
@endsection

@push('extra-js')
{{--page level js--}}
<script type="text/javascript">
    {{-- Preloader --}}
    $(window).on('load', function () {
        $('.preloader img').fadeOut();
        $('.preloader').fadeOut();
    });
    {{-- end of Preloader --}}
</script>
{{--end of page level js--}}
@endpush

@section('main-content')

    <div class="row">
        <div class="err-cont">
            <div class="col-md-6 col-md-offset-3 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1">
                <div class="row">
                    <div class="col-xs-12 col-sm-6 col-sm-push-6">
                        <div class="error_type text-center hidden-lg hidden-md hidden-sm">500</div>
                        <p class="error text-center hidden-lg hidden-md hidden-sm">error</p>

                        <div class="text-center server_img"><img src="{{ asset('img/pages/500.png') }}" alt="server break">
                        </div>
                    </div>
                    <div class="col-xs-12 col-sm-6 col-sm-pull-6">
                        <div class="text-center">
                            <div class="error_type hidden-xs">500</div>
                            <p class="error hidden-xs">error</p>

                            <div class="error_msg"><p>Oops! Something wrong with Internal Server</p></div>
                            <hr class="seperator">
                            <a href="{{ url('/') }}" class="btn btn-primary">Go Home</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection