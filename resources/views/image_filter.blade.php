@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link type="text/css" href="{{ asset('vendors/cssgram/css/cssgram.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{ asset('vendors/dropify/css/dropify.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" href="{{ asset('css/custom_css/image_filters.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script type="text/javascript" src="{{ asset('vendors/dropify/js/dropify.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom_js/image_filters.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    <h1>
        Image Filters
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#"> Gallery </a>
        </li>
        <li class="active">
            Image Filters
        </li>
    </ol>
</section>
{{-- Main content --}}
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-picture-o"></i> Image Filters (Aden - Hudson)
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-12 text-center">
                                <figure id="imgfigure1">
                                    <input type="file" class=" dropify" id="filter-1" data-show-remove="false"
                                           data-allowed-file-extensions="jpg png tif gif"
                                           data-default-file="img/gallery/full/16.jpg') }}"/>
                                </figure>
                            </div>
                            <div class="col-md-12 col-xs-12 text-center">
                                <ul class="pagination">
                                    <li data-filter1="aden">
                                        <figure class="aden text-center">
                                            <img class="temp_path1" src="{{ asset('img/gallery/full/16.jpg') }}" height="75"
                                                 alt="aden image">
                                            <span>Aden</span>
                                        </figure>
                                    </li>
                                    <li data-filter1="brannan">
                                        <figure class="brannan text-center">
                                            <img class="temp_path1" src="{{ asset('img/gallery/full/16.jpg') }}" height="75"
                                                 alt="brannan image">
                                            <span>Brannan</span>
                                        </figure>
                                    </li>
                                    <li data-filter1="brooklyn">
                                        <figure class="brooklyn text-center">
                                            <img class="temp_path1" src="{{ asset('img/gallery/full/16.jpg') }}" height="75"
                                                 alt="brooklyn image">
                                            <span>Brooklyn</span>
                                        </figure>
                                    </li>
                                    <li data-filter1="clarendon">
                                        <figure class="clarendon text-center">
                                            <img class="temp_path1" src="{{ asset('img/gallery/full/16.jpg') }}" height="75"
                                                 alt="clarendon image">
                                            <span>Clarendon</span>
                                        </figure>
                                    </li>
                                    <li data-filter1="earlybird">
                                        <figure class="earlybird text-center">
                                            <img class="temp_path1" src="{{ asset('img/gallery/full/16.jpg') }}" height="75"
                                                 alt="earlybird image">
                                            <span class="font-ccc">Earlybird</span>
                                        </figure>
                                    </li>
                                    <li data-filter1="gingham">
                                        <figure class="gingham text-center">
                                            <img class="temp_path1" src="{{ asset('img/gallery/full/16.jpg') }}" height="75"
                                                 alt="gingham image">
                                            <span>Gingham</span>
                                        </figure>
                                    </li>
                                    <li data-filter1="hudson">
                                        <figure class="hudson text-center">
                                            <img class="temp_path1" src="{{ asset('img/gallery/full/16.jpg') }}" height="75"
                                                 alt="hudson image">
                                            <span>Hudson</span>
                                        </figure>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-picture-o"></i> Image Filters (Inkwell - Perpetua)
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-12 text-center">
                                <figure id="imgfigure2">
                                    <input type="file" class=" dropify" id="filter-2"
                                           data-allowed-file-extensions="jpg png tif gif"
                                           data-default-file="img/gallery/full/15.jpg') }}"
                                           data-show-remove="false"/>
                                </figure>
                            </div>
                            <div class="col-md-12 col-xs-12 text-center">
                                <ul class="pagination">
                                    <li data-filter2="inkwell">
                                        <figure class="inkwell text-center">
                                            <img class="temp_path2" src="{{ asset('img/gallery/full/15.jpg') }}" height="75"
                                                 alt="inkwell image">
                                            <span>Inkwell</span>
                                        </figure>
                                    </li>
                                    <li data-filter2="lark">
                                        <figure class="lark text-center">
                                            <img class="temp_path2" src="{{ asset('img/gallery/full/15.jpg') }}" height="75"
                                                 alt="lark image">
                                            <span class="font-ccc">Lark</span>
                                        </figure>
                                    </li>
                                    <li data-filter2="lofi">
                                        <figure class="lofi text-center">
                                            <img class="temp_path2" src="{{ asset('img/gallery/full/15.jpg') }}" height="75"
                                                 alt="lofi image">
                                            <span>Lofi</span>
                                        </figure>
                                    </li>
                                    <li data-filter2="mayfair">
                                        <figure class="mayfair text-center">
                                            <img class="temp_path2" src="{{ asset('img/gallery/full/15.jpg') }}" height="75"
                                                 alt="mayfair image">
                                            <span>Mayfair</span>
                                        </figure>
                                    </li>
                                    <li data-filter2="moon">
                                        <figure class="moon text-center">
                                            <img class="temp_path2" src="{{ asset('img/gallery/full/15.jpg') }}" height="75"
                                                 alt="moon image">
                                            <span>Moon</span>
                                        </figure>
                                    </li>
                                    <li data-filter2="nashville">
                                        <figure class="nashville text-center">
                                            <img class="temp_path2" src="{{ asset('img/gallery/full/15.jpg') }}" height="75"
                                                 alt="nashville image">
                                            <span>Nashville</span>
                                        </figure>
                                    </li>
                                    <li data-filter2="perpetua">
                                        <figure class="perpetua text-center">
                                            <img class="temp_path2" src="{{ asset('img/gallery/full/15.jpg') }}" height="75"
                                                 alt="perpetua image">
                                            <span>Perpetua</span>
                                        </figure>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-picture-o"></i> Image Filters (Rise - Xpro2)
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-md-12 col-xs-12">
                            <div class="col-lg-6 col-lg-offset-3 col-md-8 col-md-offset-2 col-xs-12 text-center">
                                <figure id="imgfigure3">
                                    <input type="file" class=" dropify" id="filter-3"
                                           data-allowed-file-extensions="jpg png tif gif"
                                           data-default-file="img/gallery/full/5.jpg') }}" data-show-remove="false"/>
                                </figure>
                            </div>
                            <div class="col-md-12 col-xs-12 text-center">
                                <ul class="pagination">
                                    <li data-filter3="rise">
                                        <figure class="rise text-center">
                                            <img class="temp_path3" src="{{ asset('img/gallery/full/5.jpg') }}" height="75"
                                                 alt="rise image">
                                            <span>Rise</span>
                                        </figure>
                                    </li>
                                    <li data-filter3="slumber">
                                        <figure class="slumber text-center">
                                            <img class="temp_path3" src="{{ asset('img/gallery/full/5.jpg') }}" height="75"
                                                 alt="slumber image">
                                            <span>Slumber</span>
                                        </figure>
                                    </li>
                                    <li data-filter3="toaster">
                                        <figure class="toaster text-center">
                                            <img class="temp_path3" src="{{ asset('img/gallery/full/5.jpg') }}" height="75"
                                                 alt="toaster image">
                                            <span class="font-ccc">Toaster</span>
                                        </figure>
                                    </li>
                                    <li data-filter3="valencia">
                                        <figure class="valencia text-center">
                                            <img class="temp_path3" src="{{ asset('img/gallery/full/5.jpg') }}" height="75"
                                                 alt="valencia image">
                                            <span>Valencia</span>
                                        </figure>
                                    </li>
                                    <li data-filter3="walden">
                                        <figure class="walden text-center">
                                            <img class="temp_path3" src="{{ asset('img/gallery/full/5.jpg') }}" height="75"
                                                 alt="walden image">
                                            <span>Walden</span>
                                        </figure>
                                    </li>
                                    <li data-filter3="willow">
                                        <figure class="willow text-center">
                                            <img class="temp_path3" src="{{ asset('img/gallery/full/5.jpg') }}" height="75"
                                                 alt="willow image">
                                            <span>Willow</span>
                                        </figure>
                                    </li>
                                    <li data-filter3="xpro2">
                                        <figure class="xpro2 text-center">
                                            <img class="temp_path3" src="{{ asset('img/gallery/full/5.jpg') }}" height="75"
                                                 alt="xpro2 image">
                                            <span>Xpro2</span>
                                        </figure>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection