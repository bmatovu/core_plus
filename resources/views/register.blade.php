@extends ("layouts.base_min")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" href="{{ asset('vendors/iCheck/css/all.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
    <link href="{{ asset('css/register2.css') }}" rel="stylesheet">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('vendors/iCheck/js/icheck.js') }}"></script>
<script src="{{ asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/backstretch.js') }}"></script>
<script src="{{ asset('js/custom_js/register.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

    <div class="row " id="form-login">

        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 register-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <h2 class="text-center">
                            Sign Up
                            <small> with</small>
                            <img src="{{ asset('img/pages/logo.png') }}" alt="logo">
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row row-bg-color">
                <div class="col-md-8 core-register">
                    <form class="form-horizontal" method="POST" action="{{ url('login') }}" id="register_form">
                        {{-- CSRF Token --}}
                        <input type="hidden" name="_token" value="sSAo7cToGJCJ2IBFgOpYbLNnqV5n8O4DdNG5jdez"/>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group">
                                    <label class="control-label" for="user_name">USER NAME</label>
                                    <div class="input-group">
                                        <input type="text" class="form-control" placeholder="User Name"
                                               name="user_name" id="user_name" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group ">
                                    <label class="control-label" for="email">EMAIL</label>
                                    <div class="input-group">
                                        <input type="text" placeholder="Email Address" class="form-control" name="email"
                                               id="email" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row password">
                            <div class="col-sm-6">
                                <div class="form-group ">
                                    <label class="control-label" for="password">PASSWORD</label>
                                    <div class="input-group">
                                        <input type="password" placeholder="Password" class="form-control"
                                               name="password" id="password"/>
                                    </div>
                                </div>
                            </div>
                            <div class="col-sm-6">
                                <div class="form-group cp-group">
                                    <label class="control-label confirm_pwd" for="password_confirm">CONFIRM PASSWORD</label>
                                    <div class="input-group pull-right">
                                        <input type="password" placeholder="Confirm Password" class="form-control"
                                               name="password_confirm" id="password_confirm"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label class="checkbox-inline sr-only" for="terms">Agree to terms and conditions</label>
                            <input type="checkbox" value="1" name="terms" id="terms"/>&nbsp;
                            <label for="terms"> I agree to <a href="#section"> Terms and Conditions</a>.</label>
                        </div>
                        <div class="form-group ">
                            <input type="submit" class="btn btn-primary" value="Sign Up"/>
                            <input type="reset" class="btn btn-default" value="Reset" id="dee1"/><br>
                            <hr>
                            <span> Already Have an account? <a href="{{ url('login') }}">Login</a></span>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="social-buttons">
                        <p class="text-center"><label>YOU CAN ALSO SIGN UP WITH</label></p>
                        <a class="btn btn-block btn-social btn-google-plus">
                            <i class="fa fa-google-plus"></i>
                            Sign Up with Google
                        </a>
                        <a class="btn btn-block btn-social btn-facebook">
                            <i class="fa fa-facebook"></i>
                            Sign Up with Facebook
                        </a>
                        <a class="btn btn-block btn-social btn-twitter">
                            <i class="fa fa-twitter"></i>
                            Sign Up with Twitter
                        </a>
                        <a class="btn btn-block btn-social btn-linkedin">
                            <i class="fa fa-linkedin"></i>
                            Sign Up with LinkedIn
                        </a>

                    </div>
                </div>
            </div>
        </div>

    </div>

@endsection