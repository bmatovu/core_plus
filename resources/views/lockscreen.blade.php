@extends ("layouts.base_min")

@section('extra-css')
    @parent
    {{--end of global css--}}
    <link href="{{ asset('css/lockscreen2.css') }}" rel="stylesheet">
    {{--end page level css--}}
@endsection

@push('extra-js')
{{--page level js--}}
<script src="{{ asset('js/lockscreen2.js') }}"></script>
{{--end of page level js--}}
@endpush

@section('main-content')

    <div class="row">
        <div class="col-md-4 col-md-offset-4 col-sm-6 col-sm-offset-3 col-xs-10 col-xs-offset-1">
            <div class="lockscreen-container">
                <div id="output"></div>
                <div class="user-name">
                    <h4 class="text-center">Nataliapery</h4>
                    <small>ADMIN</small>
                </div>
                <div class="avatar"></div>
                <div class="form-box">
                    <form action="#" method="post">
                        <div class="form">
                            <h4>
                                <small class="locked">Enter the Password to Unlock</small>
                                <small class="unlocked hidden">Unlocked</small>
                            </h4>
                            <input type="password" name="user" class="form-control" placeholder="Password">
                            <button class="btn btn-info login" id="index" type="submit">GO</button>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection