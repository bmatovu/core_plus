@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link href="{{ asset('css/menubarfold.css') }}" rel="stylesheet">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('js/custom_js/menubarfold.js') }}" type="text/javascript"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

    {{-- Content Header (Page header) --}}
    <section class="content-header">
        {{--section starts--}}
        <h1>
            Menubar Fold
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-fw fa-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">Layouts</a>
            </li>
            <li class="active">
                <a href="{{ url('menubarfold') }}">
                    Menubar Fold
                </a>
            </li>
        </ol>
    </section>
    <section class="content">
        <div class="outer">
            <h2>Code</h2>
            <pre><code class="language-markup">&lt;nav class=&quot;leftmenubar-fold&quot;&gt;...&lt;/nav&gt;</code></pre>
        </div>
        <div class="col-lg-12">
            <p class="text-justify">
                Sed sed blandit urna. Proin ac sem nisl. Mauris risus orci, tristique eget velit at,
                congue euismod
                lacus. Curabitur id purus sit amet urna rutrum bibendum ac at quam. In hendrerit enim eu turpis
                molestie, et euismod tellus viverra. Suspendisse molestie at leo sit amet volutpat. Integer augue
                libero, scelerisque vitae luctus ac, consequat et arcu. Nullam malesuada turpis eu ullamcorper
                tincidunt. Integer aliquam felis eget neque facilisis ornare.
            </p>

            <p>
                Integer pharetra vitae dolor vel
                elementum. In nisl risus, dignissim non fermentum ac, pretium sit amet dui. Phasellus fringilla orci
                sapien, vel lacinia mi dapibus ut. Donec euismod congue nulla, in porttitor sapien. Pellentesque
                facilisis luctus adipiscing.
            </p>

            <p class="text-justify">
                Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Nam massa eros, dictum ut imperdiet eget, laoreet
                placerat orci. Aliquam eget neque neque. Donec dictum, enim convallis gravida fringilla, velit sem
                condimentum nunc, in pretium libero est sit amet elit. Nam ut arcu ac eros commodo rutrum ac nec
                purus. Fusce sodales pulvinar odio, vulputate fringilla ligula bibendum sit amet. Duis risus neque,
                molestie tincidunt odio vel, sodales vulputate mauris. Sed adipiscing justo tristique enim pharetra,
                nec ultricies metus sagittis.Duis varius id massa ut pellentesque.
            </p>

            <p> Nulla commodo erat eu mi aliquet
                aliquam. Vivamus commodo massa et nunc ullamcorper, vel pharetra purus consequat. Suspendisse a
                neque quis nibh dictum posuere ac et enim. Aliquam sit amet accumsan erat. Nullam euismod elit
                tellus, vel luctus enim luctus feugiat. Vestibulum quis placerat ipsum, porta vehicula massa. Etiam
                nec risus ac lacus gravida tincidunt. Vivamus eu ante vehicula, aliquam nisl et, suscipit ipsum.
                Vivamus velit nulla, tincidunt ac risus et, congue lobortis mauris. In condimentum consectetur
                purus, vel adipiscing felis sollicitudin vitae. Phasellus luctus, ligula eu tempor ullamcorper,
                lectus elit posuere augue, eget tempus lacus nibh a purus. Ut risus velit, adipiscing eu leo quis,
                vestibulum porttitor nunc. Sed sed blandit urna. Proin ac sem nisl.
            </p>

            <p> Mauris risus orci, tristique
                eget velit at, congue euismod lacus. Curabitur id purus sit amet urna rutrum bibendum ac at quam. In
                hendrerit enim eu turpis molestie, et euismod tellus viverra. Suspendisse molestie at leo sit amet
                volutpat. et euismod tellus viverra.
            </p>

            <div class="clearfix"></div>
            <p class="text-justify">
                Lorem ipsum
                dolor sit amet, consectetur adipiscing elit. Nam massa eros, dictum ut imperdiet eget, laoreet
                placerat orci. Aliquam eget neque neque. Donec dictum, enim convallis gravida fringilla, velit sem
                condimentum nunc, in pretium libero est sit amet elit. Nam ut arcu ac eros commodo rutrum ac nec
                purus. Fusce sodales pulvinar odio, vulputate fringilla ligula bibendum sit amet. Duis risus neque,
                molestie tincidunt odio vel, sodales vulputate mauris. Sed adipiscing justo tristique enim pharetra,
                nec ultricies metus sagittis. Duis varius id massa ut pellentesque. Nulla commodo erat eu mi aliquet
                aliquam. Vivamus commodo massa et nunc ullamcorper, vel pharetra purus consequat.
            </p>

            <div class="clearfix"></div>
        </div>
        <div class="fixedchatdiv animated fadeInRight">
            <div id="right">
                <div id="slim-scroll">
                    <div class="rightsidebar-right">
                        <div class="rightsidebar-right-content">
                            <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                                <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                            </h5>
                            <ul class="list-unstyled margin-none">
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-primary"></i> Alanis
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-primary"></i> Rolando
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-primary"></i> Marlee
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-warning"></i> Marlee
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-muted"></i> Cielo
                                    </a>
                                </li>
                                <li class="rightsidebar-contact-wrapper">
                                    <a class="rightsidebar-contact" href="#">
                                        <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                             class="img-circle pull-right" alt="avatar-image">
                                        <i class="fa fa-circle text-xs text-muted"></i> Charlene
                                    </a>
                                </li>
                            </ul>
                            <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                                <i class="fa fa-fw fa-group"></i> Recent Activities
                            </h5>

                            <div>
                                <ul class="list-unstyled">
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-warning fa-fw text-primary"></i> Server Not
                                            Responding
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order
                                            Placed
                                        </a>
                                    </li>
                                    <li class="rightsidebar-notification">
                                        <a href="#">
                                            <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                        </a>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="fixedchaticon">
            <a href="#" class="fa-stack fa-lg">
                <i class="fa fa-circle fa-stack-2x"></i>
                <i class="fa fa-comments fa-stack-1x fa-inverse"></i>
            </a>
            <span class="badge badge-danger chatlabel">7</span>
        </div>
        {{-- /.inner --}}
        {{-- /#right --}}
    </section>
    {{-- /.content --}}

@endsection