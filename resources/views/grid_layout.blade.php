@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}

{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
    <section class="content-header">
        {{--section starts--}}
        <h1>
            Grid Layout
        </h1>
        <ol class="breadcrumb">
            <li>
                <a href="{{ url('/') }}">
                    <i class="fa fa-fw fa-home"></i> Dashboard
                </a>
            </li>
            <li>
                <a href="#">
                    UI Features
                </a>
            </li>
            <li class="active">
                Grid layout
            </li>
        </ol>
    </section>
    <section class="content">
        {{--main content--}}
        <div class="row">
            <div class="col-md-12 ">
                <div class="panel panel-primary">
                    <div class="panel-heading">
                        <h3 class="panel-title">
                            <i class="fa fa-fw fa-columns"></i> Responsive Grid Examples
                        </h3>
                            <span class="pull-right hidden-xs">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                    </div>
                    <div class="panel-body" id="slim1">
                        <div class="row">
                            <div class="col-md-12">
                                <p>
                                    This demostrates Bootstrap Grid system and how it responds to different screen
                                    sizes.
                                </p>
                                <div class="panel-heading">
                                    <p class="visible-lg">
                                        lg indicates that the large grid displaying. The grid stacks horizontally
                                        &lt; 1200px.
                                    </p>
                                    <p class="visible-md">
                                        md indicates that the medium grid displaying. The grid stacks horizontally
                                        &lt; 992px.
                                    </p>
                                    <p class="visible-sm">
                                        sm indicates that the small grid displaying. The grid stacks horizontally
                                        &lt; 768px.
                                    </p>
                                    <p class="visible-xs">
                                        xs indicates that the extra small grid displaying. This grid is always
                                        horizontal.
                                    </p>
                                </div>
                            </div>
                            <div class="col-md-12 col-xs-12">
                                <div class="col-lg-4 col-md-1 col-sm-3 col-xs-4 text-center grid-success">
                                    <span class="visible-lg">.col-lg-4</span>
                                    <span class="visible-md">.col-md-1</span>
                                    <span class="visible-sm">.col-sm-3</span>
                                    <span class="visible-xs">.col-xs-4</span>
                                </div>
                                <div class="col-lg-4 col-md-5 col-sm-3 col-xs-4 text-center grid-info">
                                    <span class="visible-lg">.col-lg-4</span>
                                    <span class="visible-md">.col-md-5</span>
                                    <span class="visible-sm">.col-sm-3</span>
                                    <span class="visible-xs">.col-xs-4</span>
                                </div>
                                <div class="col-lg-4 col-md-6 col-sm-6 col-xs-4 text-center grid-danger">
                                    <span class="visible-lg">.col-lg-4</span>
                                    <span class="visible-md">.col-md-6</span>
                                    <span class="visible-sm">.col-sm-6</span>
                                    <span class="visible-xs">.col-xs-4</span>
                                </div>
                                <div class="grid-section">
                                    <h3>xs Grid</h3>
                                    <div class="col-xs-5 text-center grid-success">
                                        <div>.col-xs-5</div>
                                    </div>
                                    <div class="col-xs-4 text-center grid-info">
                                        <div>.col-xs-4</div>
                                    </div>
                                    <div class="col-xs-3 text-center grid-danger">
                                        <div>.col-xs-3</div>
                                    </div>
                                </div>
                                {{-- end row --}}
                                <div class="grid-section">
                                    <h3>sm Grid</h3>
                                    <div class="col-sm-2 text-center grid-success">
                                        <div>.col-sm-2</div>
                                    </div>
                                    <div class="col-sm-4 text-center grid-info">
                                        <div>.col-sm-4</div>
                                    </div>
                                    <div class="col-sm-6 text-center grid-danger">
                                        <div>.col-sm-6</div>
                                    </div>
                                </div>
                                {{-- end row --}}
                                <div class="grid-section grid-selection1">
                                    <h3>md Grid</h3>
                                    <div class="col-md-2 text-center grid-success">
                                        <div>.col-md-2</div>
                                    </div>
                                    <div class="col-md-4 text-center grid-info">
                                        <div>.col-md-4</div>
                                    </div>
                                    <div class="col-md-6 text-center grid-danger">
                                        <div>.col-md-6</div>
                                    </div>
                                </div>
                                {{-- end row --}}
                                <div class="grid-section grid-selection2">
                                    <h3>lg Grid</h3>
                                    <div class="col-lg-4 text-center grid-success">
                                        <div>.col-lg-4</div>
                                    </div>
                                    <div class="col-lg-4 text-center grid-info">
                                        <div>.col-lg-4</div>
                                    </div>
                                    <div class="col-lg-4 text-center grid-danger">
                                        <div>.col-lg-4</div>
                                    </div>
                                </div>
                                {{-- end row --}}
                            </div>
                            {{-- end row --}}
                        </div>
                    </div>
                </div>
            </div>
        </div>
        {{--main content ends--}}
        <div id="right">
            <div id="slim2">
                <div class="rightsidebar-right">
                    <div class="rightsidebar-right-content">
                        <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                            <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                        </h5>
                        <ul class="list-unstyled margin-none">
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-primary"></i> Alanis
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-primary"></i> Rolando
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-primary"></i> Marlee
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-warning"></i> Marlee
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-muted"></i> Cielo
                                </a>
                            </li>
                            <li class="rightsidebar-contact-wrapper">
                                <a class="rightsidebar-contact" href="#">
                                    <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                         class="img-circle pull-right" alt="avatar-image">
                                    <i class="fa fa-circle text-xs text-muted"></i> Charlene
                                </a>
                            </li>
                        </ul>
                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                            <i class="fa fa-fw fa-tasks"></i> Tasks
                        </h5>
                        <ul class="list-unstyled m-t-25">
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 1</strong>
                                        <small class="pull-right text-muted">
                                            40% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-success" role="progressbar"
                                             aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 2</strong>
                                        <small class="pull-right text-muted">
                                            20% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-info" role="progressbar"
                                             aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 3</strong>
                                        <small class="pull-right text-muted">
                                            60% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-warning" role="progressbar"
                                             aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div>
                                    <p>
                                        <strong>Task 4</strong>
                                        <small class="pull-right text-muted">
                                            80% Complete
                                        </small>
                                    </p>
                                    <div class="progress progress-xs progress-striped active">
                                        <div class="progress-bar progress-bar-danger" role="progressbar"
                                             aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                             style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                        <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                            <i class="fa fa-fw fa-group"></i> Recent Activities
                        </h5>
                        <div>
                            <ul class="list-unstyled">
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                    </a>
                                </li>
                                <li class="rightsidebar-notification">
                                    <a href="#">
                                        <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    {{-- /.content --}}

@endsection