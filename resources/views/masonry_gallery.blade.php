@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/fancybox/jquery.fancybox.css') }}" media="screen"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link href="{{ asset('css/animated-masonry-gallery.css') }}" rel="stylesheet" type="text/css"/>
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('js/jquery.isotope.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/fancybox/jquery.fancybox.pack.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/fancybox/helpers/jquery.fancybox-buttons.js') }}" type="text/javascript"></script>
{{-- Add fancyBox main JS and CSS files --}}
<script type="text/javascript" src="{{ asset('vendors/fancybox/jquery.fancybox.js') }}"></script>
<script src="{{ asset('js/animated-masonry-gallery.js') }}" type="text/javascript"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    <h1>
        Masonry Gallery
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#"> Gallery</a>
        </li>
        <li class="active">
            Masonry Gallery
        </li>
    </ol>
</section>
{{-- Main content --}}
<section class="content">
    <div class="content gallery">
        <div class="row" id="slim">
            <div id="gallery">
                <div class="row m-b-10">
                    <div class="col-md-5 col-xs-12" id="gallery-header-center-left-title">
                        All Galleries
                    </div>
                    <div class="pull-right">
                        <div class="col-xs-12">
                            <button type="button" id="filter-all" class="btn btn-responsive btn-info btn-xs">
                                All
                            </button>
                            <button type="button" id="filter-studio"
                                    class="btn btn-responsive btn-primary btn-xs">Studio
                            </button>
                            <button type="button" id="filter-landscape"
                                    class="btn btn-responsive btn-success btn-xs">Landscape
                            </button>
                        </div>
                    </div>
                </div>
                <div id="gallery-content">
                    <div id="gallery-content-center">
                        <a class="fancybox img-responsive" href="img/gallery/full/1.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/full/1.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/2.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/2.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/3.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/3.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/4.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/4.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/5.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/5.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/6.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/6.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/7.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/7.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/8.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/8.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/9.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/9.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/10.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/10.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/11.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/11.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/12.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/12.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/13.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/13.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/14.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/14.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/15.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/15.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/16.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/16.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/17.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/17.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/18.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/18.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/19.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/19.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/20.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/20.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/21.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/21.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/22.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/22.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/23.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/23.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/24.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/24.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/25.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/25.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/26.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/26.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/27.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/27.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/28.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/28.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/29.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/29.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/30.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/30.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/31.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/31.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/32.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/32.jpg') }}" class="all studio"/>
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/33.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/33.jpg') }}" class="all landscape">
                        </a>
                        <a class="fancybox img-responsive" href="img/gallery/full/34.jpg') }}"
                           data-fancybox-group="gallery" title="Lorem ipsum dolor sit amet">
                            <img alt="gallery" src="{{ asset('img/gallery/thumbs/34.jpg') }}" class="all studio"/>
                        </a>
                    </div>
                </div>
                {{-- .images-box --}}
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                    40% Complete (success)
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                    20% Complete
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                    60% Complete (warning)
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                    80% Complete (danger)
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection