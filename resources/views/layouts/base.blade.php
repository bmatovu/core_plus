<!DOCTYPE html>
<html>

<head>
    @include('includes.head')
</head>

@section('body')

    <body class="skin-coreplus">

    <div class="preloader">
        <div class="loader_img"><img src="{{ asset('img/loader.gif') }}" alt="loading..." height="64" width="64"></div>
    </div>

    {{-- header logo: style can be found in header--}}
    <header class="header">
        @include('includes.header')
    </header>

    <div class="row horizontal_menu">
        @if(in_array('layout_horizontal_menu', $select))
            @include('includes.horizontal_menu')
        @endif
    </div>

    {{-- Before Main Content --}}
    <div>
        @yield('pre-main-content')
    </div>

    {{-- wrapper --}}
    <div class="wrapper row-offcanvas row-offcanvas-left">

        @if(in_array('layout_horizontal_menu', $select))
            {{-- No sidebar --}}
        @else
            {{-- Left side column. contains the logo and sidebar --}}
            <aside class="left-side sidebar-offcanvas">
                @if(in_array('menubarfold', $select))
                    @include('includes.sidebar_fold')
                @else
                    @include('includes.sidebar')
                @endif
            </aside>
        @endif

        {{-- Right-side contains main page content --}}
        <aside class="right-side">
            @yield('main-content')
        </aside>

        {{-- footer --}}
        @include('includes.footer')

    </div>

    {{-- After Main Content --}}
    <div>
        @yield('post-main-content')
    </div>

    <div id="qn"></div>
    @include('includes.scripts')

    </body>

@show

</html>