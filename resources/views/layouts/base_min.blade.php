<!DOCTYPE html>
<html>

<head>
    @include('includes.head_min')
</head>

@section('body')

    <body @if(!empty($body_cls))class="{{ $body_cls }}"@endif >

    <div class="preloader">
        <div class="loader_img"><img src="{{ asset('img/loader.gif') }}" alt="loading..." height="64" width="64"></div>
    </div>

    {{-- Before Main Content --}}
    <div>
        @yield('pre-main-content')
    </div>

    <div class="container">
        @yield('main-content')
    </div>

    {{-- After Main Content --}}
    <div>
        @yield('post-main-content')
    </div>

    @include('includes.scripts_min')

    </body>

@show

</html>