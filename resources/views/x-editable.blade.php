@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link href="{{ asset('vendors/x-editable/css/bootstrap-editable.css') }}" rel="stylesheet" type="text/css"/>
    <link href="{{ asset('vendors/x-editable/css/typeahead.js-bootstrap.css') }}" rel="stylesheet" type="text/css"/>
    <link rel="stylesheet" href="{{ asset('vendors/iCheck/css/all.css') }}" type="text/css"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link href="{{ asset('css/inlinedit.css') }}" rel="stylesheet" type="text/css"/>
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('vendors/jquery-mockjax/js/jquery.mockjax.js') }}"></script>
<script src="{{ asset('vendors/moment/js/moment.min.js') }}"></script>
<script src="{{ asset('vendors/x-editable/js/bootstrap-editable.js') }}"></script>
<script src="{{ asset('vendors/x-editable/js/typeahead.js') }}"></script>
<script src="{{ asset('vendors/x-editable/js/typeaheadjs.js') }}"></script>
<script src="{{ asset('vendors/x-editable/js/address.js') }}"></script>
<script src="{{ asset('vendors/iCheck/js/icheck.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/demo-mock.js') }}"></script>
<script src="{{ asset('js/demo.js') }}"></script>
<script src="{{ asset('js/custom_js/x-editable.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    <h1>X-Editable</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
        <li class="active">
            <a href="{{ url('x-editable') }}">
                X-Editable</a>
        </li>
    </ol>
</section>
{{-- Main content --}}
<section class="content">
    <div class="row">
        <div class="col-lg-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-eyedropper"></i> X-Editable
                    </h3>
                </div>
                <div class="panel-body">
                    <div class="row">
                        <div class="col-sm-6 m-t-10">
                            <div class="form-group">
                                <label class="m-t-10"><input type="checkbox" id="autoopen" class="custom-checkbox"/>
                                    Auto-open next field</label>
                                <button id="enable" class="btn btn-success btn-sm">Enable / Disable</button>
                            </div>

                        </div>
                        <div class="col-sm-5">
                            <form method="get" id="frm" class="form-inline" action="#">
                                <div class="form-group">
                                    <label for="c" class="m-t-6 m-t-14 pull-left">Mode</label>
                                    <select name="c" id="c" class="form-control input-sm m-l-10 m-t-10 pull-left">
                                        <option value="popup">Popup</option>
                                        <option value="inline">Inline</option>
                                    </select>
                                    <button type="submit" class="btn btn-responsive btn-warning btn-sm m-l-10 m-t-10 pull-left">
                                        Refresh
                                    </button>
                                </div>
                            </form>
                        </div>
                    </div>
                    <table id="user" class="table table-bordered table-striped m-t-10">
                        <tbody>
                        <tr>
                            <td class="table_simple">Simple text field</td>
                            <td class="table_superuser">
                                <a href="#" id="username" data-type="text" data-pk="1"
                                   data-title="Enter username" class="editable editable-click"
                                   data-original-title="" title="">superuser</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Empty text field, required</td>
                            <td>
                                <a href="#" id="firstname" data-type="text" data-pk="1" data-placement="top"
                                   data-placeholder="Required" data-title="Enter your firstname"
                                   class="editable editable-click editable-empty" data-original-title=""
                                   title="">Change It</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Combodate (date)</td>
                            <td>
                                <a href="#" id="dob" data-type="combodate" data-value="1984-05-15"
                                   data-format="YYYY-MM-DD" data-viewformat="DD/MM/YYYY"
                                   data-template="D / MMM / YYYY" data-pk="1" data-title="Select Date of birth"
                                   class="editable editable-click">15/05/1984</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Combodate (datetime)</td>
                            <td>
                                <a href="#" id="event" data-type="combodate"
                                   data-template="D / MMM / YYYY , HH:mm" data-format="YYYY-MM-DD HH:mm"
                                   data-viewformat="MMM/D/YYYY, HH:mm" data-pk="1"
                                   data-title="Setup event date and time"
                                   class="editable editable-click editable-empty">Empty</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Text area</td>
                            <td>
                                <a href="#" id="comments" data-type="textarea" data-pk="1"
                                   data-placeholder="Your comments here..." data-title="Enter comments"
                                   class="editable editable-pre-wrapped editable-click">awesome user!</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Twitter typeahead.js</td>
                            <td>
                                <a href="#" id="state2" data-type="typeaheadjs" data-pk="1"
                                   data-placement="top" data-title="Start typing State.."
                                   class="editable editable-click">California</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Checklist</td>
                            <td>
                                <a href="#" id="fruits" data-type="checklist" data-value="2,3"
                                   data-title="Select fruits" class="editable editable-click">
                                    peach
                                    <br>apple</a>
                            </td>
                        </tr>
                        <tr>
                            <td>Custom input, several fields</td>
                            <td>
                                <a id="address" data-type="address" data-pk="1"
                                   data-title="Please, fill address" class="editable editable-click"
                                   data-original-title="" title=""> <b>Moscow</b> , Lenina st., bld. 12
                                </a>
                            </td>
                        </tr>
                        </tbody>
                    </table>
                </div>
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20" alt="avatar-image"
                                     class="img-circle pull-right">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20" alt="avatar-image"
                                     class="img-circle pull-right">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20" alt="avatar-image"
                                     class="img-circle pull-right">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20" alt="avatar-image"
                                     class="img-circle pull-right">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20" alt="avatar-image"
                                     class="img-circle pull-right">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20" alt="avatar-image"
                                     class="img-circle pull-right">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20" alt="avatar-image"
                                     class="img-circle pull-right">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">40% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">20% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">60% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">80% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>

@endsection