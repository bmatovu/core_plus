@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}">
    <link href="{{ asset('css/timeline.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/timeline2.css') }}" rel="stylesheet"/>
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script type="text/javascript" src="{{ asset('vendors/wow/js/wow.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom_js/time_line.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    {{--section starts--}}
    <h1>TimeLine</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">UI Components</a>
        </li>
        <li class="active">
            Timeline
        </li>
    </ol>
</section>
{{--section ends--}}
{{--section ends--}}
<section class="content">
    {{--main content--}}
    <div class="row">
        <div class="col-md-12 timeline_panel">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-clock-o"></i> Timeline
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    {{--timeline--}}
                    <div>
                        <ul class="timeline">
                            <li>
                                <div class="timeline-badge primary wow lightSpeedIn center">
                                    <i class="fa fa-fw fa-floppy-o"></i>
                                </div>
                                <div class="timeline-panel wow slideInLeft" style="display:inline-block;">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title">We are a multi national company now</h4>
                                        <p>
                                            <small class="text-primary">11 hours ago via Twitter</small>
                                        </p>
                                    </div>
                                    <div class="timeline-body">
                                        <p>
                                            Lorem Ipsum is simply dummy, vidis lio, in elementis m� pra quem �
                                            amistosis quis leo..
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-badge danger wow lightSpeedIn center">
                                    <i class="fa fa-fw fa-check-square-o"></i>
                                </div>
                                <div class="timeline-panel wow slideInRight">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title">We won the best website award</h4>
                                        <p>
                                            <small class="text-danger">May 08, 2016</small>
                                        </p>
                                    </div>
                                    <div class="timeline-body">
                                        <p>Lorem Ipsum is simply dummy, vidis litro abertis.</p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="timeline-badge info wow lightSpeedIn center">
                                    <i class="fa fa-fw fa-credit-card"></i>
                                </div>
                                <div class="timeline-panel wow slideInLeft">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title">Hired our first employee</h4>
                                        <p>
                                            <small class="text-info">June 10, 2005</small>
                                        </p>
                                    </div>
                                    <div class="timeline-body">
                                        <p>
                                            Lorem Ipsum is simply dummy, vidis litro abertis. Consetis
                                            adipiscings elitis. Pra uium u num gostis.
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li class="timeline-inverted">
                                <div class="timeline-badge warning wow lightSpeedIn center">
                                    <i class="fa fa-fw fa-indent"></i>
                                </div>
                                <div class="timeline-panel wow lightSpeedIn">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title">Rented office space</h4>
                                        <p>
                                            <small class="text-warning">Jan 05, 2002</small>
                                        </p>
                                    </div>
                                    <div class="timeline-body">
                                        <p>
                                            Lorem Ipsum is simply dummy, vidis litro abertis. Cais bolis eu num
                                            gostis.
                                        </p>
                                    </div>
                                </div>
                            </li>
                            <li>
                                <div class="timeline-badge default wow lightSpeedIn center">
                                    <i class="fa fa-fw fa-thumbs-o-up"></i>
                                </div>
                                <div class="timeline-panel wow slideInLeft">
                                    <div class="timeline-heading">
                                        <h4 class="timeline-title">Planning to open an office</h4>
                                        <p>
                                            <small class="text-default-gray">jan 02, 2017</small>
                                        </p>
                                    </div>
                                    <div class="timeline-body">
                                        <p>
                                            Lorem Ipsum is simply dummy, vidis litro abertis. Consetis
                                            adipiscings elitis. Pra l� , depois divoltis porris, s m� pra quem �
                                            amistosis.
                                        </p>
                                        <hr>
                                        <div class="btn-group">
                                            <button type="button" class="btn btn-primary btn-sm dropdown-toggle"
                                                    data-toggle="dropdown">
                                                <i class="glyphicon glyphicon-cog"></i>
                                                <span class="caret"></span>
                                            </button>
                                            <ul class="dropdown-menu" role="menu">
                                                <li>
                                                    <a href="#">Action</a>
                                                </li>
                                                <li>
                                                    <a href="#">Another action</a>
                                                </li>
                                                <li>
                                                    <a href="#">Something else here</a>
                                                </li>
                                                <li class="divider"></li>
                                                <li>
                                                    <a href="#">Separated link</a>
                                                </li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                            </li>
                        </ul>
                    </div>
                    {{--timeline ends--}}
                </div>
            </div>
        </div>
    </div>
    {{--main content ends--}}
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">40% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">20% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">60% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">80% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection