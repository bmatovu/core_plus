@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link href="{{ asset('vendors/toastr/css/toastr.min.css') }}" rel="stylesheet"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/iCheck/css/minimal/blue.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom_css/toastr_notificatons.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('vendors/toastr/js/toastr.min.js') }}"></script>
<script src="{{ asset('vendors/iCheck/js/icheck.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom_js/toastr_notifications.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    <h1>Toastr Notifications</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">UI Features</a>
        </li>
        <li class="active">
            <a href="{{ url('toastr_notifications') }}">Toastr Notifications</a>
        </li>
    </ol>
</section>
{{-- Main content --}}
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-bell-o"></i> Toastr Notifications
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="portlet-body">
                        <div class="row">
                            <div class="col-md-12">
                                <h4><i class="fa fa-fw fa-info-circle text-info message"></i> When changing
                                    toastr position clear all toasts to see the effect</h4></div>
                            <div class="col-md-3">
                                <div class="form-group">
                                    <label class="control-label" for="title">Title</label>
                                    <input id="title" type="text" class="form-control"
                                           value="Toastr Notifications" placeholder="Enter a title ...">
                                </div>
                                <div class="form-group">
                                    <label class="control-label" for="message">Message</label>
                                            <textarea class="form-control resize_vertical" id="message" rows="3"
                                                      placeholder="Enter a message ...">Gnome &amp; Growl type non-blocking notifications</textarea>
                                </div>
                                <div class="form-group">
                                    <div>
                                        <label for="closeButton">
                                            <input id="closeButton" type="checkbox" checked=""
                                                   class="input-small custom-checkbox"> Close Button
                                        </label>
                                    </div>
                                    <div>
                                        <label for="addBehaviorOnToastClick">
                                            <input id="addBehaviorOnToastClick" type="checkbox"
                                                   class="input-small custom-checkbox"> Add behavior on toast
                                            click
                                        </label>
                                    </div>
                                    <div>
                                        <label for="debugInfo">
                                            <input id="debugInfo" type="checkbox"
                                                   class="input-small custom-checkbox"> Debug
                                        </label>
                                    </div>
                                    <div>
                                        <label for="progressBar">
                                            <input id="progressBar" type="checkbox"
                                                   class="input-small custom-checkbox"/> Progress Bar
                                        </label>
                                    </div>
                                    <div>
                                        <label for="preventDuplicates">
                                            <input id="preventDuplicates" type="checkbox"
                                                   class="input-small custom-checkbox"/> Prevent Duplicates
                                        </label>
                                    </div>
                                    <div>
                                        <label for="addClear">
                                            <input id="addClear" type="checkbox"
                                                   class="input-small custom-checkbox"/> Force clearing button
                                        </label>
                                    </div>
                                    <div>
                                        <label for="newestOnTop">
                                            <input id="newestOnTop" type="checkbox"
                                                   class="input-small custom-checkbox"/> Newest on top
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3 ">
                                <div class="form-group" id="toastTypeGroup">
                                    <label class="toast-type">Toast Type</label>
                                    <div>
                                        <label>
                                            <input type="radio" name="toasts" class="custom-radio"
                                                   value="success" checked=""> Success
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" name="toasts" class="custom-radio" value="info">
                                            Info
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" name="toasts" class="custom-radio"
                                                   value="warning"> Warning
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" name="toasts" class="custom-radio"
                                                   value="error"> Error
                                        </label>
                                    </div>
                                </div>
                                <div class="form-group" id="positionGroup">
                                    <label class="position-type">Position</label>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-top-left"> Top Left
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-top-right" checked=""> Top Right
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-top-center"/> Top Center
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-top-full-width"> Top Full Width
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-bottom-left"> Bottom Left
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-bottom-right"> Bottom Right
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-bottom-center"/> Bottom Center
                                        </label>
                                    </div>
                                    <div>
                                        <label>
                                            <input type="radio" class="custom-radio" name="positions"
                                                   value="toast-bottom-full-width"> Bottom Full Width
                                        </label>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="controls">
                                    <div class="form-group">
                                        <label class="control-label" for="showEasing">Show Easing</label>
                                        <select class="form-control input-small" id="showEasing">
                                            <option>swing</option>
                                            <option>linear</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="hideEasing">Hide Easing</label>
                                        <select id="hideEasing" class="form-control input-small">
                                            <option>swing</option>
                                            <option>linear</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="showMethod">Show Method</label>
                                        <select id="showMethod" class="form-control input-small">
                                            <option value="show">show</option>
                                            <option value="fadeIn">fadeIn</option>
                                            <option value="slideDown">slideDown</option>
                                        </select>
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="hideMethod">Hide Method</label>
                                        <select class="form-control input-small" id="hideMethod">
                                            <option value="">hide</option>
                                            <option value="fadeOut">fadeOut</option>
                                            <option value="slideUp">slideUp</option>
                                        </select>
                                    </div>
                                </div>
                            </div>
                            <div class="col-md-3">
                                <div class="controls">
                                    <div class="form-group">
                                        <label class="control-label" for="showDuration">Show Duration</label>
                                        <input id="showDuration" type="text" placeholder="ms"
                                               class="form-control input-small" value="1000">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="hideDuration">Hide Duration</label>
                                        <input id="hideDuration" type="text" placeholder="ms"
                                               class="form-control input-small" value="1000">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="timeOut">Time out</label>
                                        <input id="timeOut" type="text" placeholder="ms"
                                               class="form-control input-small" value="5000">
                                    </div>
                                    <div class="form-group">
                                        <label class="control-label" for="timeOut">Extended time out</label>
                                        <input id="extendedTimeOut" type="text" placeholder="ms"
                                               class="form-control input-small" value="1000">
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-xs-12">
                                <div class="col-sm-2 col-xs-6">
                                    <button type="button" class="btn btn-default btn-raised toastrshow"
                                            id="showtoast">
                                        Show Toast
                                    </button>
                                </div>
                                <div class="col-sm-2 col-xs-6">
                                    <button type="button" class="btn btn-default btn-raised toastrshow"
                                            id="cleartoasts">
                                        Clear Toasts
                                    </button>
                                </div>
                                <div class="col-sm-2 col-xs-6">
                                    <button type="button" class="btn btn-default btn-raised toastrshow"
                                            id="clearlasttoast">
                                        Clear Last Toast
                                    </button>
                                </div>
                            </div>
                        </div>
                        <br/>
                        <div class="row margin-top-10">
                            <div class="col-md-12">
                                <pre id="toastrOptions"></pre>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">40% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">20% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">60% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">80% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection