@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" href="{{ asset('vendors/animate/animate.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link href="{{ asset('css/animate1.css') }}" rel="stylesheet"/>
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script src="{{ asset('vendors/select2/js/select2.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom_js/transitions_custom.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    {{--section starts--}}
    <h1>Transitions</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">UI Components</a>
        </li>
        <li class="active">
            Transitions
        </li>
    </ol>
</section>
{{--section ends--}}
<section class="content">
    {{--main content--}}
    <div class="row animated fadeInDown">
        {{--row starts--}}
        <div class="col-md-12">
            {{--md starts--}}
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-bell-o"></i> Transition Effects
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="col-md-12">
                        <div class="row text-center">
                            <div class="col-md-12">
                                <h5> Click on the Icon to see an Animation</h5>
                            </div>
                        </div>
                        <div class="panel-body text-center">
                            {{--content starts--}}
                            <div class="wrap">
                                <div class="row ">
                                    <div class="col-md-2 col-sm-12">
                                        <div id="tad" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Tada</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div id="fls" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Flash</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div id="shk" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Shake</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div id="sw" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Swing</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div id="pul" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Pulse</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2 col-sm-12">
                                        <div id="wobb" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Wobble</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--first row end--}}
                                {{--second row start--}}
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div id="jell" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Jello</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="rubber" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Rubber Band</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="bi" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce In</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="bid" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce In Down</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="bil" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce In Left</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="bir" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce In Right</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--second row end--}}
                                {{--third row start--}}
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div id="bo" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce Out</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="bol" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce Out Left</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="bor" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce Out Right</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="bod" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Bounce Out Down</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fi" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeIn</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fid" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeIn Down</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--third row end--}}
                                {{--fourth row start--}}
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div id="filb" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeIn LeftBig</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="firb" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeIn RightBig</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fo" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeOut</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fol" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeOut Left</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="for" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeOut Right</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fou" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FadeOut Up</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--fourth row end--}}
                                {{--fifth row start--}}
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div id="flp" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Flip</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fix" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FlipInX</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fiy" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FlipInY</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="fox" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FlipOutX</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="foy" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>FlipOutY</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="lis" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Light Speed In</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--fifth row end--}}
                                {{--sixth row start--}}
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div id="roi" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Rotate In</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="rou" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Rotate Out</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="lso" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>light Speed Out</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="riur" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Rotate In Up Right</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="riul" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Rotate In Up Left</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="roul" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Rotate Out Up Left</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--sixth row end--}}
                                {{--seventh row start--}}
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div id="siu" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Slide In Up</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="sid" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Slide In Down</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="sil" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Slide In Left</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="sir" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Slide In Right</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="sou" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Slide Out Up</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="sor" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Slide Out Right</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--seventh row end--}}
                                {{--eight row start--}}
                                <div class="row ">
                                    <div class="col-md-2">
                                        <div id="zi" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Zoom In</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="zo" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Zoom Out</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="ziu" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Zoom In Up</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="hi" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Hinge</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="ri" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Roll In</p>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="col-md-2">
                                        <div id="ro" class="panel  panel-default">
                                            <div class="panel-body">
                                                {{-- Animation trigger--}}
                                                <em class="fa fa-eject fa-2x"></em>
                                                <p>Roll Out</p>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                {{--eight row end--}}
                            </div>
                        </div>
                    </div>
                </div> {{-- panel body ending--}}
            </div>
        </div>
    </div>
    {{--row ends--}}
    {{--main content ends--}}
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">40% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                        <span class="sr-only">40% Complete (success)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">20% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                        <span class="sr-only">20% Complete</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">60% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                        <span class="sr-only">60% Complete (warning)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">80% Complete</small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                        <span class="sr-only">80% Complete (danger)</span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">general components
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{-- /.content --}}

@endsection