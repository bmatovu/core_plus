@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
        {{-- daterange picker --}}
<link href="{{ asset('vendors/daterangepicker/css/daterangepicker.css') }}" rel="stylesheet" type="text/css"/>
<link href="{{ asset('vendors/datetimepicker/css/bootstrap-datetimepicker.min.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/datedropper/datedropper.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/timedropper/css/timedropper.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/jquerydaterangepicker/css/daterangepicker.min.css') }}">
{{--clock face css--}}
<link rel="stylesheet" type="text/css" href="{{ asset('vendors/clockpicker/css/bootstrap-clockpicker.min.css') }}">
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

<link rel="stylesheet" type="text/css" href="{{ asset('css/datepicker.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
        {{-- InputMask --}}
<script src="{{ asset('vendors/moment/js/moment.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/inputmask/inputmask/inputmask.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/inputmask/inputmask/jquery.inputmask.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/inputmask/inputmask/inputmask.date.extensions.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/inputmask/inputmask/inputmask.extensions.js') }}" type="text/javascript"></script>
{{-- date-range-picker --}}
<script src="{{ asset('vendors/daterangepicker/js/daterangepicker.js') }}" type="text/javascript"></script>
{{-- bootstrap time picker --}}
<script src="{{ asset('vendors/datetimepicker/js/bootstrap-datetimepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/clockpicker/js/bootstrap-clockpicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/jquerydaterangepicker/js/jquery.daterangepicker.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/datedropper/datedropper.js') }}" type="text/javascript"></script>
<script src="{{ asset('vendors/timedropper/js/timedropper.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/custom_js/datepickers.js') }}" type="text/javascript"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    {{--section starts--}}
    <h1>
        Date pickers
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">Forms</a>
        </li>
        <li>
            <a>
                Date pickers
            </a>
        </li>
    </ol>
</section>
{{--section ends--}}
<section class="content">
    {{--main content--}}
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-calendar"></i> Date and Time range picker
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="box-body">
                        {{-- Date range --}}
                        <div class="form-group">
                            <label>
                                Date picker:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="date-range0"
                                       placeholder="YYYY-MM-DD to YYYY-MM-DD"/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label>
                                Date and Time Picker:
                            </label>
                            <div class="input-group ">
                                <input type="text" class="form-control" id="dateclock"
                                       placeholder="YYYY-MM-DD HH:MM ~ YYYY-MM-DD HH:MM">
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Span instead of Input:
                            </label>
                            <div class="input-group">
                                        <span id="date-range9"
                                              style="background-color:#428BCA; color:white;padding:3px; cursor:pointer; border-radius:4px;">YYYY-MM-DD to YYYY-MM-DD</span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Date picker with Animation:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-clock-o"></i>
                                </div>
                                <input id="date-range50" size="30" value="" class="form-control"
                                       placeholder="YYYY-MM-DD to YYYY-MM-DD">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label>
                                Hotel booking:
                            </label>
                            <div class="input-group">

                                <input id="hotel-booking" class="form-control" size="60" value=""
                                       placeholder="Days Booked">
                                <span></span>
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Select backward:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input class="form-control" id="date-range26" size="30" value=""
                                       placeholder="YYYY-MM-DD to YYYY-MM-DD">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        <div class="form-group">
                            <label>
                                Single Date mode with single month:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input class="form-control" id="date-range13-2" size="40"
                                       placeholder="YYYY-MM-DD">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        <div class="form-group">
                            <label>
                                Batch mode ( week ):
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input class="form-control" id="date-range14" size="60" value=""
                                       placeholder="YYYY-MM-DD to YYYY-MM-DD">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                    </div>
                    {{-- /.form group --}}
                    {{-- time picker --}}
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-tint"></i> Date & Time Dropper
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label>
                            Date dropper:
                        </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-fw fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="departure" placeholder="DD/MM/YYYY"/>
                        </div>
                        {{-- /.input group --}}
                    </div>
                    {{-- /.form group --}}
                    <div class="form-group">
                        <label>
                            Date format:
                        </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-fw fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="departure1" placeholder="YYYY-MM-DD"/>
                        </div>
                        {{-- /.input group --}}
                    </div>
                    {{-- /.form group --}}
                    <div class="form-group">
                        <label>
                            Maximum Year:
                        </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-fw fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="departure2" placeholder="DD/MM/YYYY"/>
                        </div>
                        {{-- /.input group --}}
                    </div>
                    <div class="form-group">
                        <label>
                            Animate date dropper
                        </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-fw fa-calendar"></i>
                            </div>
                            <input type="text" class="form-control" id="departure3" placeholder="DD/MM/YYYY"/>
                        </div>
                        {{-- /.input group --}}
                    </div>
                    {{-- /.form group --}}
                    <div class="form-group">
                        <label>
                            Time dropper:
                        </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-fw fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control" id="alarm" placeholder="HH-MM"/>
                        </div>
                        {{-- /.input group --}}
                    </div>
                    {{-- /.form group --}}
                    <div class="form-group">
                        <label>
                            Meridian time dropper:
                        </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-fw fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control" id="alarm1" placeholder="HH-MM"/>
                        </div>
                        {{-- /.input group --}}
                    </div>
                    <div class="form-group">
                        <label>
                            Animate time dropper:
                        </label>
                        <div class="input-group">
                            <div class="input-group-addon">
                                <i class="fa fa-fw fa-clock-o"></i>
                            </div>
                            <input type="text" class="form-control" id="alarm2" placeholder="HH-MM"/>
                        </div>
                        {{-- /.input group --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="row">
        <div class="col-md-6">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-calendar"></i> Date Range picker
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="box-body">
                        {{-- Date range --}}
                        <div class="form-group">
                            <label>
                                Date range:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="reservation"
                                       placeholder="MM/DD/YYYY - MM/DD/YYYY"/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label>
                                Clock Picker:
                            </label>
                            <div class="input-group clockpicker" data-placement="left" data-align="top"
                                 data-autoclose="true">
                                <input type="text" class="form-control" value="Now" placeholder="HH:MM">
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Call Backs Clock Picker:
                            </label>
                            <div class="input-group clockpicker-with-callbacks">
                                <input type="text" class="form-control" value="Now" placeholder="HH:MM">
                                        <span class="input-group-addon">
                                                <span class="glyphicon glyphicon-time"></span>
                                            </span>
                            </div>
                        </div>
                        <div class="form-group">
                            <label>
                                Date and time range:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-clock-o"></i>
                                </div>
                                <input type="text" class="form-control pull-right" id="reservationtime"
                                       placeholder="MM/DD/YYYY HH:MM-MM/DD/YYYY HH:MM">
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date and time range --}}
                        <div class="form-group">
                            <label>
                                Date range button:
                            </label>
                            <div class="input-group">
                                            <span class="input-group-addon">
                                                    <i class="fa fa-calendar"></i>
                                                </span>
                                <input type="text" class="form-control" id="reportrange"
                                       placeholder="DD/MM/YYYY-DD/MM/YYYY">
                            </div>
                        </div>
                        {{-- /.form group --}}
                        {{-- time picker --}}
                    </div>
                </div>
            </div>
        </div>
        <div class="col-md-6">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-heart-o"></i> Input masks
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <div class="box-body">
                        {{-- Date dd/mm/yyyy --}}
                        <div class="form-group">
                            <label>
                                Date masks:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'dd/mm/yyyy'"
                                       data-mask/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- Date mm/dd/yyyy --}}
                        <div class="form-group">
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-calendar"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'mm/dd/yyyy'"
                                       data-mask/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- phone mask --}}
                        <div class="form-group">
                            <label>
                                US phone mask:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-phone"></i>
                                </div>
                                <input type="text" class="form-control"
                                       data-inputmask='"mask": "(999) 999-9999"' data-mask/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- phone mask --}}
                        <div class="form-group">
                            <label>
                                Intl US phone mask:
                            </label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-phone"></i>
                                </div>
                                <input type="text" class="form-control"
                                       data-inputmask="'mask': ['999-999-9999 [x99999]', '+099 99 99 9999[9]-9999']"
                                       data-mask/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                        {{-- IP mask --}}
                        <div class="form-group">
                            <label>IP mask:</label>
                            <div class="input-group">
                                <div class="input-group-addon">
                                    <i class="fa fa-fw fa-laptop"></i>
                                </div>
                                <input type="text" class="form-control" data-inputmask="'alias': 'ip'"
                                       data-mask/>
                            </div>
                            {{-- /.input group --}}
                        </div>
                        {{-- /.form group --}}
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--main content ends--}}
</section>
{{-- /.content --}}

@endsection