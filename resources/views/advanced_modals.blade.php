@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link type="text/css" rel="stylesheet" href="{{ asset('vendors/hover/css/hover-min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/animate/animate.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom_css/advanced_modals.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script type="text/javascript" src="{{ asset('js/custom_js/advanced_modals.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    {{--section starts--}}
    <h1>Advanced Modals</h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">
                UI Features
            </a>
        </li>
        <li class="active">
            <a href="{{ url('advanced_modals') }}">Advanced Modals</a>
        </li>
    </ol>
</section>
<section class="content">
    {{--main content--}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-gear"></i> Modal Styles
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <h4>Basic Modal</h4>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#basic_modal">
                        Open Basic Modal
                    </button>
                    {{-- Modal Animate Buttons --}}
                    <h4>Animated Modal Entrances</h4>
                    <div class="row">
                        <div class="col-md-12">
                            <div class="row">
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="fadeIn">Fade In
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="slideInRight">Slide
                                        In Right
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="slideInUp">Slide From
                                        Bottom
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="jello">Jello
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="bounceInLeft">Bounce
                                        In Left
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="flip">Flip
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="flipInX">Flip In
                                        Horizantlly
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="flipInY">Flip In
                                        Vertically
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="rotateInDownLeft">
                                        Rotate In
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="lightSpeedIn">Light
                                        Speed
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="wobble">Wobble
                                    </button>
                                </div>
                                <div class="col-md-3 col-sm-3">
                                    <button type="button" class="btn btn-info btn-block" data-toggle="modal"
                                            data-target="#basic_modal" data-animate-modal="zoomInDown">Zoom In
                                        Down
                                    </button>
                                </div>
                            </div>
                        </div>
                        {{-- End of Animation buttons --}}
                        <div id="basic_modal" class="modal fade animated" role="dialog">
                            <div class="modal-dialog">
                                <div class="modal-content">
                                    <div class="modal-header">
                                        <button type="button" class="close"
                                                data-dismiss="modal">&times;</button>
                                        <h4 class="modal-title">Modal Header</h4>
                                    </div>
                                    <div class="modal-body">
                                        <p>This is the text in modal</p>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                        </button>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modal position --}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-info">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-gear"></i> Modal Positions
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <h4>Position Of Modal</h4>
                    <p>The different modal positions are Top, Center, Bottom and also as a Side-bar</p>
                    <button type="button" class="btn btn-info topposition" data-toggle="modal"
                            data-target="#top_modal">Top Modal
                    </button>
                    <div id="top_modal" class="modal fade animated position_modal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                    <p>This is the modal at top of the screen</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-info centerposition" data-toggle="modal"
                            data-target="#center_modal">Center Modal
                    </button>
                    <div id="center_modal" class="modal fade animated position_modal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                    <p>This is the modal at center of the screen</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-info bottomposition" data-toggle="modal"
                            data-target="#bottom_modal">Bottom Modal
                    </button>
                    <div id="bottom_modal" class="modal fade animated position_modal" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                    <p>This is the modal at bottom of the screen</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-info sidebarmodal" data-toggle="modal"
                            data-target="#sidebar_modal">Side-bar Modal
                    </button>
                    <div id="sidebar_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                    <p>This is the text in modal</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- end of modal position --}}
    {{-- additional Modals --}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-warning">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-gear"></i> Additional Modal
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <h4>Additional Contents of a Modal</h4>
                    {{-- form-modal --}}
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#form_modal">
                        Form Modal
                    </button>
                    <div id="form_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <form role="form">
                                    <div class="modal-body">
                                        <div class="row m-t-10">
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <label class="sr-only" for="first-name">First-Name</label>
                                                    <input type="text" name="first-name" id="first-name"
                                                           placeholder="First Name" class="form-control m-t-10">
                                                </div>
                                            </div>
                                            <div class="col-md-6">
                                                <div class="input-group">
                                                    <label class="sr-only" for="last-name">Last-Name</label>
                                                    <input type="text" name="last-name" id="last-name"
                                                           placeholder="Last Name" class="form-control m-t-10">
                                                </div>
                                            </div>
                                        </div>
                                        <div class="row m-t-10">
                                            <div class="col-md-12">
                                                <div class="form-group">
                                                    <label class="sr-only" for="message">Message</label>
                                                            <textarea class="form-control resize_vertical m-t-10"
                                                                      name="message"
                                                                      placeholder="Message" rows="6"
                                                                      id="message"></textarea>
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="modal-footer">
                                        <button type="submit" class="btn btn-succes">Submit</button>
                                        <button type="reset" class="btn btn-default">Reset</button>
                                        <button type="button" class="btn btn-default" data-dismiss="modal">
                                            Close
                                        </button>
                                    </div>
                                </form>
                            </div>
                        </div>
                    </div>
                    {{-- form-modal end --}}
                    {{-- Multiple Modals --}}
                    <button type="button" class="btn btn-info" data-toggle="modal"
                            data-target="#multiple_modal1">Multiple Modals
                    </button>
                    <div id="multiple_modal1" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal 1</h4>
                                </div>
                                <div class="modal-body">
                                    Click Next to go to next modal.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-info" data-target="#multiple_modal2"
                                            data-toggle="modal" data-dismiss="modal">Next
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div id="multiple_modal2" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal 2</h4>
                                </div>
                                <div class="modal-body">
                                    This is the second modal
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- multiple modals end --}}
                    {{-- modal sizes --}}
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#large_modal">
                        Large Modal
                    </button>
                    <div id="large_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog modal-lg">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Large Modal</h4>
                                </div>
                                <div class="modal-body">
                                    This is a large modal.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#small_modal">
                        Small Modal
                    </button>
                    <div id="small_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog modal-sm">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Small Modal</h4>
                                </div>
                                <div class="modal-body">
                                    This is a small modal.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- modal sizes end --}}
                </div>
            </div>
        </div>
    </div>
    {{-- end additional modals --}}
    {{-- bootstrap components in modals --}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-primary">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-gear"></i> Modal With Bootstrap Components
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <h4>Advanced Modal</h4>
                    {{-- modal with tabs --}}
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#tabs_modal">
                        Modal With Tabs
                    </button>
                    <div id="tabs_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal With Tabs</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>Tabs in a Modal</h4>
                                    <ul class="nav nav-tabs">
                                        <li class="active">
                                            <a data-toggle="tab" href="#tab1">Tab 1</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#tab2">Tab 2</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#tab3">Tab 3</a>
                                        </li>
                                        <li>
                                            <a data-toggle="tab" href="#tab4">Tab 4</a>
                                        </li>
                                    </ul>
                                    <div class="tab-content">
                                        <div id="tab1" class="tab-pane fade in active">
                                            <h3>Tab 1</h3>
                                            <p>First Tab</p>
                                        </div>
                                        <div id="tab2" class="tab-pane fade">
                                            <h3>TAB 2</h3>
                                            <p>Second Tab</p>
                                        </div>
                                        <div id="tab3" class="tab-pane fade">
                                            <h3>TAB 3</h3>
                                            <p>Third Tab</p>
                                        </div>
                                        <div id="tab4" class="tab-pane fade">
                                            <h3>TAB 4</h3>
                                            <p>Fourth Tab</p>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- modal with tabs end --}}
                    {{-- modal with accordions --}}
                    <button type="button" class="btn btn-info" data-toggle="modal"
                            data-target="#accordions_modal">Accordions in a Modal
                    </button>
                    <div id="accordions_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal with Accordions</h4>
                                </div>
                                <div class="modal-body">
                                    <h4>Accordion In a Modal</h4>
                                    <div class="panel-group" id="accordion">
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse1">Collapsible Group 1</a>
                                                </h4>
                                            </div>
                                            <div id="collapse1" class="panel-collapse collapse in">
                                                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                                    et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                                    commodo consequat.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse2">Collapsible Group 2</a>
                                                </h4>
                                            </div>
                                            <div id="collapse2" class="panel-collapse collapse">
                                                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                                    et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                                    commodo consequat.
                                                </div>
                                            </div>
                                        </div>
                                        <div class="panel panel-default">
                                            <div class="panel-heading">
                                                <h4 class="panel-title">
                                                    <a data-toggle="collapse" data-parent="#accordion"
                                                       href="#collapse3">Collapsible Group 3</a>
                                                </h4>
                                            </div>
                                            <div id="collapse3" class="panel-collapse collapse">
                                                <div class="panel-body">Lorem ipsum dolor sit amet, consectetur
                                                    adipisicing elit, sed do eiusmod tempor incididunt ut labore
                                                    et dolore magna aliqua. Ut enim ad minim veniam, quis
                                                    nostrud exercitation ullamco laboris nisi ut aliquip ex ea
                                                    commodo consequat.
                                                </div>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- modal with accordions end--}}
                    {{-- grid in a modal --}}
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#grid_modal">
                        Modals with Grid
                    </button>
                    <div id="grid_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal with grid arrangement</h4>
                                </div>
                                <div class="modal-body">
                                    <div class="row">
                                        <div class="col-md-6">col-md-6</div>
                                        <div class="col-md-6">col-md-6</div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-12">col-md-12
                                            <div class="row">
                                                <div class="col-md-6">col-md-6</div>
                                                <div class="col-md-6">col-md-6</div>
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">col-md-6</div>
                                        <div class="col-md-6">col-md-6</div>
                                    </div>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- grid in a modal end--}}
                    {{-- fill modal --}}
                    <button type="button" class="btn btn-info" data-toggle="modal" data-target="#fill_modal">
                        Just Me
                    </button>
                    <div id="fill_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Just Me</h4>
                                </div>
                                <div class="modal-body">
                                    This modal fills the entire page.
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{-- fill modal end--}}
                </div>
            </div>
        </div>
    </div>
    {{-- bootstrap components in modals end--}}
    {{-- modals with colors --}}
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-fw fa-gear"></i> Modal Colors
                    </h3>
                            <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    <h4>Color Of Modal</h4>
                    <p>We can set any color to a modal. Here are some examples..</p>
                    <button type="button" class="btn btn-default modalcolor" data-toggle="modal"
                            data-target="#color_modal" data-modalcolor="#dcdcdc">Default Modal
                    </button>
                    <button type="button" class="btn btn-primary modalcolor" data-toggle="modal"
                            data-target="#color_modal" data-modalcolor="#428bca">Primary Modal
                    </button>
                    <button type="button" class="btn btn-info modalcolor" data-toggle="modal"
                            data-target="#color_modal" data-modalcolor="#4fc1e9">Info Modal
                    </button>
                    <button type="button" class="btn btn-success modalcolor" data-toggle="modal"
                            data-target="#color_modal" data-modalcolor="#22d69d">Success Modal
                    </button>
                    <button type="button" class="btn btn-warning modalcolor" data-toggle="modal"
                            data-target="#color_modal" data-modalcolor="#ffb65f">Warning Modal
                    </button>
                    <button type="button" class="btn btn-danger modalcolor" data-toggle="modal"
                            data-target="#color_modal" data-modalcolor="#fb8678">Danger Modal
                    </button>
                    <div id="color_modal" class="modal fade animated" role="dialog">
                        <div class="modal-dialog">
                            <div class="modal-content">
                                <div class="modal-header">
                                    <button type="button" class="close" data-dismiss="modal">&times;</button>
                                    <h4 class="modal-title">Modal Header</h4>
                                </div>
                                <div class="modal-body">
                                    <p>This is the text in modal</p>
                                </div>
                                <div class="modal-footer">
                                    <button type="button" class="btn btn-default" data-dismiss="modal">Close
                                    </button>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{-- modals with colors end--}}
    {{--main content ends--}}
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                        40% Complete (success)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                        20% Complete
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                        60% Complete (warning)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                        80% Complete (danger)
                                                    </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
</section>
{{--section ends--}}
{{-- /.content --}}

@endsection