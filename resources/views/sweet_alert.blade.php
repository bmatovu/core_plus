@extends ("layouts.base")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">

    <link rel="stylesheet" type="text/css" href="{{ asset('vendors/sweetalert2/css/sweetalert2.min.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom_css/sweet_alert2.css') }}">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script type="text/javascript" src="{{ asset('vendors/sweetalert2/js/sweetalert2.min.js') }}"></script>
<script type="text/javascript" src="{{ asset('js/custom_js/sweetalert.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

        {{-- Content Header (Page header) --}}
<section class="content-header">
    {{--section starts--}}
    <h1>
        Sweet Alert
    </h1>
    <ol class="breadcrumb">
        <li>
            <a href="{{ url('/') }}">
                <i class="fa fa-fw fa-home"></i> Dashboard
            </a>
        </li>
        <li>
            <a href="#">UI Features</a>
        </li>
        <li class="active">
            Sweet Alert
        </li>
    </ol>
</section>
{{--section ends--}}
<section class="content">
    <div class="row">
        <div class="col-md-12">
            <div class="panel panel-success">
                <div class="panel-heading">
                    <h3 class="panel-title">
                        <i class="fa fa-bell-o" aria-hidden="true"></i>
                        Sweet Alerts
                    </h3>
                      <span class="pull-right">
                                    <i class="fa fa-fw fa-chevron-up clickable"></i>
                                    <i class="fa fa-fw fa-times removepanel clickable"></i>
                                </span>
                </div>
                <div class="panel-body">
                    {{--=============panel content starting=============--}}
                    <div class="row m-a-10">
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body success_alert text-center">
                                    <h5> Success Alert <i class="fa fa-check-circle-o"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body ok_message text-center">
                                    <h5> Ok Message <i class="fa fa-thumbs-o-up"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body basicalert text-center ">
                                    <h5> Alert <i class="fa fa-bell-o"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body ip_alert text-center">
                                    <h5> Ip Alert <i class="fa fa-info-circle"></i></h5>
                                </div>
                            </div>
                        </div>

                    </div>
                    {{--first row end--}}
                    <div class="row m-a-10">
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body custom_icon text-center">
                                    <h5> Custom Image <i class="fa fa-picture-o"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body custom_html text-center">
                                    <h5> Custom Html <i class="fa fa-code"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body auto_close text-center">
                                    <h5> Alert Auto Close <i class="fa fa-magic"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body prom_alert text-center">
                                    <h5> Prompt Alert <i class="fa fa-tree"></i></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--second row end--}}
                    <div class="row m-a-10">
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body text-center" id="info-alert">
                                    <h5> Info Alert  <i class="fa fa-info"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body text-center" id="success-alert">
                                    <h5> Successfully  <i class="fa fa-check"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body text-center" id="warning-alert">
                                    <h5> Warning Alert  <i class="fa fa-exclamation"></i></h5>
                                </div>
                            </div>
                        </div>
                        <div class="col-md-3 col-sm-3">
                            <div class="panel  panel-default hvr-sweep-to-right">
                                <div class="panel-body text-center" id="danger-alert">
                                    <h5> Danger Alert  <i class="fa fa-times"></i></h5>
                                </div>
                            </div>
                        </div>
                    </div>
                    {{--third row end--}}
                    {{--=================== panel content end ======================--}}
                </div>
                {{--end of panel body--}}
            </div>
        </div>
    </div>
    <div id="right">
        <div id="slim2">
            <div class="rightsidebar-right">
                <div class="rightsidebar-right-content">
                    <h5 class="rightsidebar-right-heading rightsidebar-right-heading-first text-uppercase text-xs">
                        <i class="menu-icon  fa fa-fw fa-paw"></i> Contacts
                    </h5>
                    <ul class="list-unstyled margin-none">
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar1.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Alanis
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Rolando
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar2.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-primary"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar3.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-warning"></i> Marlee
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar4.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-danger"></i> Kamryn
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar5.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Cielo
                            </a>
                        </li>
                        <li class="rightsidebar-contact-wrapper">
                            <a class="rightsidebar-contact" href="#">
                                <img src="{{ asset('img/authors/avatar7.jpg') }}" height="20" width="20"
                                     class="img-circle pull-right" alt="avatar-image">
                                <i class="fa fa-circle text-xs text-muted"></i> Charlene
                            </a>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-tasks"></i> Tasks
                    </h5>
                    <ul class="list-unstyled m-t-25">
                        <li>
                            <div>
                                <p>
                                    <strong>Task 1</strong>
                                    <small class="pull-right text-muted">
                                        40% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-success" role="progressbar"
                                         aria-valuenow="40" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 40%">
                                                    <span class="sr-only">
                                                    40% Complete (success)
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 2</strong>
                                    <small class="pull-right text-muted">
                                        20% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-info" role="progressbar"
                                         aria-valuenow="20" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 20%">
                                                    <span class="sr-only">
                                                    20% Complete
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 3</strong>
                                    <small class="pull-right text-muted">
                                        60% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-warning" role="progressbar"
                                         aria-valuenow="60" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 60%">
                                                    <span class="sr-only">
                                                    60% Complete (warning)
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                        <li>
                            <div>
                                <p>
                                    <strong>Task 4</strong>
                                    <small class="pull-right text-muted">
                                        80% Complete
                                    </small>
                                </p>
                                <div class="progress progress-xs progress-striped active">
                                    <div class="progress-bar progress-bar-danger" role="progressbar"
                                         aria-valuenow="80" aria-valuemin="0" aria-valuemax="100"
                                         style="width: 80%">
                                                    <span class="sr-only">
                                                    80% Complete (danger)
                                                </span>
                                    </div>
                                </div>
                            </div>
                        </li>
                    </ul>
                    <h5 class="rightsidebar-right-heading text-uppercase text-xs">
                        <i class="fa fa-fw fa-group"></i> Recent Activities
                    </h5>
                    <div>
                        <ul class="list-unstyled">
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-comment fa-fw text-primary"></i> New Comment
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-twitter fa-fw text-success"></i> 3 New Followers
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-envelope fa-fw text-info"></i> Message Sent
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-tasks fa-fw text-warning"></i> New Task
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-upload fa-fw text-danger"></i> Server Rebooted
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-warning fa-fw text-primary"></i> Server Not Responding
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-shopping-cart fa-fw text-success"></i> New Order Placed
                                </a>
                            </li>
                            <li class="rightsidebar-notification">
                                <a href="#">
                                    <i class="fa fa-money fa-fw text-info"></i> Payment Received
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </div>
    {{--main content ends--}}
</section>
{{-- /.content --}}

@endsection