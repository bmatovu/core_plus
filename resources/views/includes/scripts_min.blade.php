{{-- BEGIN THEME GLOBAL SCRIPTS --}}
<script src="{{ asset('js/jquery-1.11.3.min.js') }}" type="text/javascript"></script>
<script src="{{ asset('js/bootstrap.min.js') }}" type="text/javascript"></script>
{{-- END THEME GLOBAL SCRIPTS --}}

{{-- BEGIN DYNAMIC SCRIPTS --}}
@stack('extra-js')
{{-- END DYNAMIC SCRIPTS --}}