{{-- Bootstrap --}}
<link href="{{ asset('css/bootstrap.min.css') }}" rel="stylesheet">
{{-- end of bootstrap --}}

{{-- BEGIN DYNAMIC CSS --}}
@yield('extra-css')
{{-- END DYNAMIC CSS --}}