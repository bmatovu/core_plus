<meta charset="UTF-8">
<title>
    @if(!empty($title))
        {{ $title }}
    @else
        {{ config('app.name') }} Admin Template
    @endif
</title>
<meta content='width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no' name='viewport'>
<meta content="Brian Matovu" name="author"/>

{{--CSRF Token--}}
<meta name="csrf-token" content="{{ csrf_token() }}">

@include('includes.styles')