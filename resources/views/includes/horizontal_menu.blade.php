<div class="col-md-12">
    <ul id="navigation" class="slimmenu">
        <li class="main-menu option-one"><a href="javascript:void(0)" class="menu-list">Dashboards</a>
            <ul>
                <li>
                    <a href="{{ url('/') }}" class="sub-list">
                        Dashboard V1
                    </a>
                </li>
                <li>
                    <a href="{{ url('index2') }}" class="sub-list">
                        Dashboard V2
                    </a>
                </li>
            </ul>
        </li>
        <li class="main-menu"><a href="javascript:void(0)" class="menu-list">Elements</a>
            <ul>
                <li><a href="javascript:void(0)" class="sub-list">Forms</a>
                    <ul>
                        <li>
                            <a href="{{ url('form_elements') }}">
                                Form Elements
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('form_validations') }}">
                                Form Validations
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('form_layouts') }}">
                                Form Layouts
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('form_wizards') }}">
                                Form Wizards
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('radio_checkboxes') }}">
                                Radio and Checkbox
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('advanceddate_pickers') }}">
                                Advanced Date pickers
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('x-editable') }}">
                                X-editable
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" class="sub-list">UI Features</a>
                    <ul>
                        <li>
                            <a href="{{ url('general_components') }}">
                                General Components
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('buttons') }}">
                                Buttons
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('fonts') }}">
                                Font Icons
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('advanced_modals') }}">
                                Advanced Modals
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('gridstack') }}">
                                Grid Stack
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('sweet_alert') }}">
                                Sweet Alert
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('toastr_notifications') }}">
                                Toastr Notifications
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('notifications') }}">
                                Notifications
                            </a>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0)" class="sub-list">Components</a>
                    <ul>
                        <li>
                            <a href="{{ url('timeline') }}">
                                Timeline
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('transitions') }}">
                                Transitions
                            </a>
                        </li>
                    </ul>
                </li>
                <li><a href="javascript:void(0)" class="sub-list">Calendar</a>
                    <ul>
                        <li>
                            <a href="{{ url('calendar') }}">
                                Calendar
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('calendar2') }}">
                                Advanced Calendar
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="main-menu"><a href="javascript:void(0)" class="menu-list">Data </a>
            <ul>
                <li>
                    <a href="javascript:void(0)" class="sub-list">Data Tables</a>
                    <ul class="sub-menu">
                        <li>
                            <a href="{{ url('simple_tables') }}">
                                Simple tables
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('datatables') }}">
                                Data Tables
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('advanced_datatables') }}">
                                Advanced Tables
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('responsive_datatables') }}">
                                Responsive DataTables
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('bootstrap_tables') }}">
                                Bootstrap Tables
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" class="sub-list">Charts</a>
                    <ul>
                        <li>
                            <a href="{{ url('flot_charts') }}">
                                Flot Charts
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('nvd3_charts') }}">
                                NVD3 Charts
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('circle_sliders') }}">
                                Circle Sliders
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('chartjs_charts') }}">
                                Chartjs Charts
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('dimple_charts') }}">
                                Dimple Charts
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('amcharts') }}">
                                Amcharts
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('chartist') }}">
                                Chartist Charts
                            </a>
                        </li>
                    </ul>
                </li>
                <li>
                    <a href="javascript:void(0)" class="sub-list">Gallery</a>
                    <ul>
                        <li>
                            <a href="{{ url('masonry_gallery') }}">
                                <i class="fa fa-fw fa-file-image-o"></i> Masonry Gallery
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('multiplefile_upload') }}">
                                <i class="fa fa-fw fa-cloud-upload"></i> Multiple File Upload
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('dropify') }}">
                                <i class="fa fa-fw fa-dropbox"></i> Dropify
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('image_hover') }}">
                                <i class="fa fa-file-image-o"></i> Image Hover
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('image_filter') }}">
                                <i class="fa fa-filter"></i> Image Filter
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('image_magnifier') }}">
                                <i class="fa  fa-search-plus"></i> Image Magnifier
                            </a>
                        </li>
                    </ul>
                </li>
            </ul>
        </li>
        <li class="main-menu"><a href="javascript:void(0)" class="menu-list">Maps </a>
            <ul>
                <li>
                    <a href="{{ url('google_maps') }}" class="sub-list">
                        Google Maps
                    </a>
                </li>
                <li>
                    <a href="{{ url('vector_maps') }}" class="sub-list">
                        Vector Maps
                    </a>
                </li>
                <li>
                    <a href="{{ url('advanced_maps') }}" class="sub-list">
                        Advanced Maps
                    </a>
                </li>
            </ul>
        </li>
        <li class="main-menu"><a href="javascript:void(0)" class="menu-list">Pages </a>
            <ul>
                <li>
                    <a href="{{ url('lockscreen') }}" class="sub-list">
                        Lockscreen
                    </a>
                </li>
                <li>
                    <a href="{{ url('invoice') }}" class="sub-list">
                        Invoice
                    </a>
                </li>
                <li>
                    <a href="{{ url('blank') }}" class="sub-list">
                        Blank
                    </a>
                </li>
                <li>
                    <a href="{{ url('login') }}" class="sub-list">
                        Login
                    </a>
                </li>
                <li>
                    <a href="{{ url('register') }}" class="sub-list">
                        Register
                    </a>
                </li>
                <li>
                    <a href="{{ url('404') }}" class="sub-list">
                        404 Error
                    </a>
                </li>
                <li>
                    <a href="{{ url('500') }}" class="sub-list">
                        500 Error
                    </a>
                </li>
            </ul>
        </li>
        <li><a href="javascript:void(0)" class="menu-list">Layouts </a>
            <ul>
                <li>
                    <a href="{{ url('menubarfold') }}" class="sub-list">
                        Menubar Fold
                    </a>
                </li>
                <li>
                    <a href="{{ url('layout_horizontal_menu') }}" class="sub-list">
                        Horizonta Menu
                    </a>
                </li>
                <li>
                    <a href="{{ url('boxed') }}" class="sub-list">
                        Boxed Layout
                    </a>
                </li>
                <li>
                    <a href="{{ url('layout_fixed_header') }}" class="sub-list">
                        Fixed Header
                    </a>
                </li>
                <li>
                    <a href="{{ url('layout_boxed_fixed_header') }}" class="sub-list">
                        Boxed &amp; Fixed Header
                    </a>
                </li>
                <li>
                    <a href="{{ url('layout_fixed') }}" class="sub-list">
                        Fixed Header &amp; Menu
                    </a>
                </li>
            </ul>
        </li>
    </ul>
</div>