<link rel="shortcut icon" href="{{ asset('img/favicon.ico') }}"/>

{{-- BEGIN GLOBAL CSS --}}
<link type="text/css" href="{{ asset('css/app.css') }}" rel="stylesheet"/>
<link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
{{-- END GLOBAL CSS --}}

{{-- BEGIN DYNAMIC CSS --}}
@yield('extra-css')
{{-- END DYNAMIC CSS --}}