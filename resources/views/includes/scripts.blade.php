{{-- HTML5 Shim and Respond.js IE8 support of HTML5 elements and media queries --}}
{{-- WARNING: Respond.js doesn't work if you view the page via file:// --}}
<!--[if lt IE 9]>
<script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
<script src="https://oss.maxcdn.com/libs/respond.js/1.3.0/respond.min.js"></script>
<![endif]-->

{{-- BEGIN CORE PLUGINS --}}

{{-- END CORE PLUGINS --}}

{{-- BEGIN THEME GLOBAL SCRIPTS --}}
<script src="{{ asset('js/app.js') }}" type="text/javascript"></script>
{{-- END THEME GLOBAL SCRIPTS --}}

{{-- BEGIN THEME LAYOUT SCRIPTS --}}

{{-- END THEME LAYOUT SCRIPTS --}}

{{-- BEGIN DYNAMIC SCRIPTS --}}
@stack('extra-js')
{{-- END DYNAMIC SCRIPTS --}}