{{-- sidebar: style can be found in sidebar--}}
<section class="sidebar">
    <div id="menu" role="navigation">
        <div class="nav_profile">
            <div class="media profile-left">
                <a class="pull-left profile-thumb" href="#">
                    <img src="{{ asset('img/authors/avatar1.jpg') }}" class="img-circle" alt="User Image">
                </a>
                {{--
                <div class="content-profile">
                    <h4 class="media-heading">
                        Nataliapery
                    </h4>
                    <ul class="icon-list">
                        <li>
                            <a href="{{ url('/user_list') }}">
                                <i class="fa fa-fw fa-user"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('lockscreen') }}">
                                <i class="fa fa-fw fa-lock"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('edit_user') }}">
                                <i class="fa fa-fw fa-gear"></i>
                            </a>
                        </li>
                        <li>
                            <a href="{{ url('login') }}">
                                <i class="fa fa-fw fa-sign-out"></i>
                            </a>
                        </li>
                    </ul>
                </div>
                --}}
            </div>
        </div>
        <ul class="navigation">
            <li @if(in_array('index', $select)) class="active" id="active" @endif>
                <a href="{{ url('index') }}">
                    <i class="menu-icon fa fa-fw fa-home"></i>
                    {{--<span class="mm-text ">Dashboard V1</span>--}}
                </a>
            </li>
            <li @if(in_array('index2', $select)) class="active" id="active" @endif>
                <a href="{{ url('index2') }}">
                    <i class="menu-icon fa fa-fw fa-tachometer"></i>
                    {{--<span class="mm-text ">Dashboard V2</span>--}}
                </a>
            </li>
            <li class="menu-dropdown @if(in_array('forms', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-check-square"></i>
                    {{--<span>Forms</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('form_elements', $select)) class="active" id="active" @endif>
                        <a href="{{ url('form_elements') }}">
                            <i class="fa fa-fw fa-fire"></i> Form Elements
                        </a>
                    </li>
                    <li @if(in_array('form_editors', $select)) class="active" id="active" @endif>
                        <a href="{{ url('form_editors') }}">
                            <i class="fa fa-fw fa-file-text-o"></i> Form Editors
                        </a>
                    </li>
                    <li @if(in_array('form_validations', $select)) class="active" id="active" @endif>
                        <a href="{{ url('form_validations') }}">
                            <i class="fa fa-fw fa-warning"></i> Form Validations
                        </a>
                    </li>
                    <li @if(in_array('form_layouts', $select)) class="active" id="active" @endif>
                        <a href="{{ url('form_layouts') }}">
                            <i class="fa fa-fw fa-fire"></i> Form Layouts
                        </a>
                    </li>
                    <li @if(in_array('form_wizards', $select)) class="active" id="active" @endif>
                        <a href="{{ url('form_wizards') }}">
                            <i class="fa fa-fw fa-cog"></i> Form Wizards
                        </a>
                    </li>
                    <li @if(in_array('complex_forms', $select)) class="active" id="active" @endif>
                        <a href="{{ url('complex_forms') }}">
                            <i class="fa fa-fw fa-newspaper-o"></i> Complex Forms
                        </a>
                    </li>
                    <li @if(in_array('complex_forms2', $select)) class="active" id="active" @endif>
                        <a href="{{ url('complex_forms2') }}">
                            <i class="fa fa-fw fa-newspaper-o"></i> Complex Forms 2
                        </a>
                    </li>
                    <li @if(in_array('radio_checkboxes', $select)) class="active" id="active" @endif>
                        <a href="{{ url('radio_checkboxes') }}">
                            <i class="fa fa-fw fa-check-square-o"></i> Radio and Checkbox
                        </a>
                    </li>
                    <li @if(in_array('dropdowns', $select)) class="active" id="active" @endif>
                        <a href="{{ url('dropdowns') }}">
                            <i class="fa fa-fw fa-chevron-circle-down"></i> Drop Downs
                        </a>
                    </li>
                    <li @if(in_array('datepicker', $select)) class="active" id="active" @endif>
                        <a href="{{ url('datepicker') }}">
                            <i class="fa fa-fw fa-calendar-o"></i> Date pickers
                        </a>
                    </li>
                    <li @if(in_array('advanceddate_pickers', $select)) class="active" id="active" @endif>
                        <a href="{{ url('advanceddate_pickers') }}">
                            <i class="fa fa-fw fa-calendar"></i> Advanced Date pickers
                        </a>
                    </li>
                    <li @if(in_array('x-editable', $select)) class="active" id="active" @endif>
                        <a href="{{ url('x-editable') }}">
                            <i class="fa fa-fw fa-eyedropper"></i> X-editable
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('ui-features', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-desktop"></i>
                    {{--<span>UI Features</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('general_components', $select)) class="active" id="active" @endif>
                        <a href="{{ url('general_components') }}">
                            <i class="fa fa-fw fa-plug"></i> General Components
                        </a>
                    </li>
                    <li @if(in_array('pickers', $select)) class="active" id="active" @endif>
                        <a href="{{ url('pickers') }}">
                            <i class="fa fa-fw fa-paint-brush"></i> Pickers
                        </a>
                    </li>
                    <li @if(in_array('buttons', $select)) class="active" id="active" @endif>
                        <a href="{{ url('buttons') }}">
                            <i class="fa fa-fw fa-delicious"></i> Buttons
                        </a>
                    </li>
                    <li @if(in_array('tabs_accordions', $select)) class="active" id="active" @endif>
                        <a href="{{ url('tabs_accordions') }}">
                            <i class="fa fa-fw fa-copy"></i> Tabs &amp; Accordions
                        </a>
                    </li>
                    <li @if(in_array('fonts', $select)) class="active" id="active" @endif>
                        <a href="{{ url('fonts') }}">
                            <i class="fa fa-fw fa-font"></i>
                            <span>Font Icons</span>
                            <span class="fa arrow"></span>
                        </a>
                        {{--@if(in_array('fontawesome_icons', $select) || in_array('glyphicons', $select) || in_array('simple_line_icons', $select))--}}
                        {{--<ul class="sub-menu sub-submenu">--}}
                        {{--<li @if(in_array('fontawesome_icons', $select)) class="active" id="active" @endif>--}}
                        {{--<a href="{{ url('fontawesome_icons') }}">--}}
                        {{--<i class="fa fa-fw fa-sitemap"></i> Fontawesome Icons--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li @if(in_array('glyphicons', $select)) class="active" id="active" @endif>--}}
                        {{--<a href="{{ url('glyphicons') }}">--}}
                        {{--<i class="fa fa-fw fa-sitemap"></i> Glyphicons--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--<li @if(in_array('simple_line_icons', $select)) class="active" id="active" @endif>--}}
                        {{--<a href="{{ url('simple_line_icons') }}">--}}
                        {{--<i class="fa fa-fw fa-sitemap"></i> Simple Line Icons--}}
                        {{--</a>--}}
                        {{--</li>--}}
                        {{--</ul>--}}
                        {{--@endif--}}
                    </li>
                    <li @if(in_array('grid_layout', $select)) class="active" id="active" @endif>
                        <a href="{{ url('grid_layout') }}"><i class="fa fa-fw fa-columns"></i> Grid Layout
                        </a>
                    </li>
                    <li @if(in_array('advanced_modals', $select)) class="active" id="active" @endif>
                        <a href="{{ url('advanced_modals') }}">
                            <i class="fa fa-fw fa-suitcase"></i> Advanced Modals
                        </a>
                    </li>
                    <li @if(in_array('gridstack', $select)) class="active" id="active" @endif>
                        <a href="{{ url('gridstack') }}">
                            <i class="fa fa-fw fa-slack"></i> Grid Stack
                        </a>
                    </li>
                    <li @if(in_array('tags_input', $select)) class="active" id="active" @endif>
                        <a href="{{ url('tags_input') }}">
                            <i class="fa fa-fw fa-tag"></i> Tags Input
                        </a>
                    </li>
                    <li @if(in_array('nestable_list', $select)) class="active" id="active" @endif>
                        <a href="{{ url('nestable_list') }}">
                            <i class="fa fa-fw fa-navicon"></i> Nestable List
                        </a>
                    </li>
                    <li @if(in_array('sweet_alert', $select)) class="active" id="active" @endif>
                        <a href="{{ url('sweet_alert') }}">
                            <i class="fa fa-fw fa-bell"></i> Sweet Alert
                        </a>
                    </li>
                    <li @if(in_array('toastr_notifications', $select)) class="active" id="active" @endif>
                        <a href="{{ url('toastr_notifications') }}">
                            <i class="fa fa-fw fa-desktop"></i> Toastr Notifications
                        </a>
                    </li>
                    <li @if(in_array('notifications', $select)) class="active" id="active" @endif>
                        <a href="{{ url('notifications') }}">
                            <i class="fa fa-fw fa-flag"></i> Notifications
                        </a>
                    </li>
                    <li @if(in_array('session_timeout', $select)) class="active" id="active" @endif>
                        <a href="{{ url('session_timeout') }}">
                            <i class="fa fa-fw fa-rocket"></i> Session Timeout
                        </a>
                    </li>
                    <li @if(in_array('draggable_portlets', $select)) class="active" id="active" @endif>
                        <a href="{{ url('draggable_portlets') }}">
                            <i class="fa fa-fw fa-random"></i> Draggable Portlets
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('ui-components', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-briefcase"></i>
                    {{--<span>UI Components</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('timeline', $select)) class="active" id="active" @endif>
                        <a href="{{ url('timeline') }}">
                            <i class="fa fa-fw fa-clock-o"></i> Timeline
                        </a>
                    </li>
                    <li @if(in_array('transitions', $select)) class="active" id="active" @endif>
                        <a href="{{ url('transitions') }}">
                            <i class="fa fa-fw fa-star-half-empty"></i> Transitions
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('tables', $select)) active @endif">
                <a href="#"> <i class="menu-icon fa fa-table"></i>
                    {{--<span>Tables</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('simple_tables', $select)) class="active" id="active" @endif>
                        <a href="{{ url('simple_tables') }}">
                            <i class="fa fa-fw fa-tasks"></i> Simple tables
                        </a>
                    </li>
                    <li @if(in_array('datatables', $select)) class="active" id="active" @endif>
                        <a href="{{ url('datatables') }}">
                            <i class="fa fa-fw fa-database"></i> Data Tables
                        </a>
                    </li>
                    <li @if(in_array('advanced_datatables', $select)) class="active" id="active" @endif>
                        <a href="{{ url('advanced_datatables') }}">
                            <i class="fa fa-fw fa-table"></i> Advanced Tables
                        </a>
                    </li>
                    <li @if(in_array('responsive_datatables', $select)) class="active" id="active" @endif>
                        <a href="{{ url('responsive_datatables') }}">
                            <i class="fa fa-fw fa-table"></i> Responsive DataTables
                        </a>
                    </li>
                    <li @if(in_array('bootstrap_tables', $select)) class="active" id="active" @endif>
                        <a href="{{ url('bootstrap_tables') }}">
                            <i class="fa fa-fw fa-table"></i> Bootstrap Tables
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('charts', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-bar-chart-o"></i>
                    {{--<span>Charts</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('flot_charts', $select)) class="active" id="active" @endif>
                        <a href="{{ url('flot_charts') }}">
                            <i class="fa fa-fw fa-area-chart"></i> Flot Charts
                        </a>
                    </li>
                    <li @if(in_array('nvd3_charts', $select)) class="active" id="active" @endif>
                        <a href="{{ url('nvd3_charts') }}">
                            <i class="fa fa-fw fa-line-chart"></i> NVD3 Charts
                        </a>
                    </li>
                    <li @if(in_array('circle_sliders', $select)) class="active" id="active" @endif>
                        <a href="{{ url('circle_sliders') }}">
                            <i class="fa fa-fw fa-sun-o"></i> Circle Sliders
                        </a>
                    </li>
                    <li @if(in_array('chartjs_charts', $select)) class="active" id="active" @endif>
                        <a href="{{ url('chartjs_charts') }}">
                            <i class="fa fa-fw fa-pie-chart"></i> Chartjs Charts
                        </a>
                    </li>
                    <li @if(in_array('dimple_charts', $select)) class="active" id="active" @endif>
                        <a href="{{ url('dimple_charts') }}">
                            <i class="fa fa-fw fa-area-chart"></i> Dimple Charts
                        </a>
                    </li>
                    <li @if(in_array('amcharts', $select)) class="active" id="active" @endif>
                        <a href="{{ url('amcharts') }}">
                            <i class="fa fa-fw fa-line-chart"></i> Amcharts
                        </a>
                    </li>
                    <li @if(in_array('chartist', $select)) class="active" id="active" @endif>
                        <a href="{{ url('chartist') }}">
                            <i class="fa fa-fw fa-bar-chart"></i> Chartist Charts
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('calendars', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-fw fa-calendar"></i>
                    {{--<span>Calendars</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('calendar', $select)) class="active" id="active" @endif>
                        <a href="{{ url('calendar') }}">
                            <i class=" menu-icon fa fa-fw fa-calendar"></i>
                            <span>Calendar</span>
                            <small class="badge">7</small>
                        </a>
                    </li>
                    <li @if(in_array('calendar2', $select)) class="active" id="active" @endif>
                        <a href="{{ url('calendar2') }}">
                            <i class=" menu-icon fa fa-fw fa-calendar-o"></i>
                            <span>Advanced Calendar</span>
                            <small class="badge">6</small>
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('gallery', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-fw fa-photo"></i>
                    {{--<span>Gallery</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('masonry_gallery', $select)) class="active" id="active" @endif>
                        <a href="{{ url('masonry_gallery') }}">
                            <i class="fa fa-fw fa-file-image-o"></i> Masonry Gallery
                        </a>
                    </li>
                    <li @if(in_array('multiplefile_upload', $select)) class="active" id="active" @endif>
                        <a href="{{ url('multiplefile_upload') }}">
                            <i class="fa fa-fw fa-cloud-upload"></i> Multiple File Upload
                        </a>
                    </li>
                    <li @if(in_array('dropify', $select)) class="active" id="active" @endif>
                        <a href="{{ url('dropify') }}">
                            <i class="fa fa-fw fa-dropbox"></i> Dropify
                        </a>
                    </li>
                    <li @if(in_array('image_hover', $select)) class="active" id="active" @endif>
                        <a href="{{ url('image_hover') }}">
                            <i class="fa fa-file-image-o"></i> Image Hover
                        </a>
                    </li>
                    <li @if(in_array('image_filter', $select)) class="active" id="active" @endif>
                        <a href="{{ url('image_filter') }}">
                            <i class="fa fa-filter"></i> Image Filter
                        </a>
                    </li>
                    <li @if(in_array('image_magnifier', $select)) class="active" id="active" @endif>
                        <a href="{{ url('image_magnifier') }}">
                            <i class="fa  fa-search-plus"></i> Image Magnifier
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('users', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-fw fa-users"></i>
                    {{--<span>Users</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('user_list', $select)) class="active" id="active" @endif>
                        <a href="{{ url('user_list') }}">
                            <i class="fa fa-list" aria-hidden="true"></i> Users List
                        </a>
                    </li>
                    <li @if(in_array('addnew_user', $select)) class="active" id="active" @endif>
                        <a href="{{ url('addnew_user') }}">
                            <i class="fa fa-fw fa-user"></i> Add New User
                        </a>
                    </li>
                    <li @if(in_array('user_profile', $select)) class="active" id="active" @endif>
                        <a href="{{ url('user_profile') }}">
                            <i class="fa fa-fw fa-user-md"></i> View Profile
                        </a>
                    </li>
                    <li @if(in_array('deleted_users', $select)) class="active" id="active" @endif>
                        <a href="{{ url('deleted_users') }}">
                            <i class="fa fa-fw fa-times"></i> Deleted Users
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('maps', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-map-marker"></i>
                    {{--<span>Maps</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('google_maps', $select)) class="active" id="active" @endif>
                        <a href="{{ url('google_maps') }}">
                            <i class="fa fa-fw fa-globe"></i> Google Maps
                        </a>
                    </li>
                    <li @if(in_array('vector_maps', $select)) class="active" id="active" @endif>
                        <a href="{{ url('vector_maps') }}">
                            <i class="fa fa-fw fa-map-marker"></i> Vector Maps
                        </a>
                    </li>
                    <li @if(in_array('advanced_maps', $select)) class="active" id="active" @endif>
                        <a href="{{ url('advanced_maps') }}">
                            <i class="fa fa-fw fa-location-arrow"></i> Advanced Maps
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('pages', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-files-o"></i>
                    {{--<span>Pages</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('lockscreen', $select)) class="active" id="active" @endif>
                        <a href="{{ url('lockscreen') }}">
                            <i class="fa fa-fw fa-lock"></i> Lockscreen
                        </a>
                    </li>
                    <li @if(in_array('lockscreen2', $select)) class="active" id="active" @endif>
                        <a href="{{ url('lockscreen2') }}">
                            <i class="fa fa-fw fa-lock"></i> Lockscreen V2
                        </a>
                    </li>
                    <li @if(in_array('invoice', $select)) class="active" id="active" @endif>
                        <a href="{{ url('invoice') }}">
                            <i class="fa fa-fw fa-newspaper-o"></i> Invoice
                        </a>
                    </li>
                    <li @if(in_array('blank', $select)) class="active" id="active" @endif>
                        <a href="{{ url('blank') }}">
                            <i class="fa fa-fw fa-file-o"></i> Blank
                        </a>
                    </li>
                    <li @if(in_array('login', $select)) class="active" id="active" @endif>
                        <a href="{{ url('login') }}">
                            <i class="fa fa-fw fa-sign-in"></i> Login
                        </a>
                    </li>
                    <li @if(in_array('login2', $select)) class="active" id="active" @endif>
                        <a href="{{ url('login2') }}">
                            <i class="fa fa-fw fa-sign-in"></i> Login V2
                        </a>
                    </li>
                    <li @if(in_array('register', $select)) class="active" id="active" @endif>
                        <a href="{{ url('register') }}">
                            <i class="fa fa-fw fa-sign-in"></i> Register
                        </a>
                    </li>
                    <li @if(in_array('register2', $select)) class="active" id="active" @endif>
                        <a href="{{ url('register2') }}">
                            <i class="fa fa-fw fa-sign-in"></i> Register V2
                        </a>
                    </li>
                    <li @if(in_array('404', $select)) class="active" id="active" @endif>
                        <a href="{{ url('404') }}">
                            <i class="fa fa-fw fa-unlink"></i> 404 Error
                        </a>
                    </li>
                    <li @if(in_array('500', $select)) class="active" id="active" @endif>
                        <a href="{{ url('500') }}">
                            <i class="fa fa-fw fa-frown-o"></i> 500 Error
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown @if(in_array('layouts', $select)) active @endif">
                <a href="#">
                    <i class="menu-icon fa fa-th"></i>
                    {{--<span>Layouts</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li @if(in_array('menubarfold', $select)) class="active" id="active" @endif>
                        <a href="{{ url('menubarfold') }}">
                            <i class="fa fa-fw fa-list-alt"></i> Menubar Fold
                        </a>
                    </li>
                    <li @if(in_array('layout_horizontal_menu', $select)) class="active" id="active" @endif>
                        <a href="{{ url('layout_horizontal_menu') }}">
                            <i class="fa fa-fw fa-bars"></i> Horizontal Menu
                        </a>
                    </li>
                    <li @if(in_array('boxed', $select)) class="active" id="active" @endif>
                        <a href="{{ url('boxed') }}">
                            <i class="fa fa-fw fa-th-large"></i> Boxed Layout
                        </a>
                    </li>
                    <li @if(in_array('layout_fixed_header', $select)) class="active" id="active" @endif>
                        <a href="{{ url('layout_fixed_header') }}">
                            <i class="fa fa-fw fa-th-list"></i> Fixed Header
                        </a>
                    </li>
                    <li @if(in_array('layout_boxed_fixed_header', $select)) class="active" id="active" @endif>
                        <a href="{{ url('layout_boxed_fixed_header') }}">
                            <i class="fa fa-fw fa-th"></i> Boxed &amp; Fixed Header
                        </a>
                    </li>
                    <li @if(in_array('layout_fixed', $select)) class="active" id="active" @endif>
                        <a href="{{ url('layout_fixed') }}">
                            <i class="fa fa-fw fa-indent"></i> Fixed Header &amp; Menu
                        </a>
                    </li>
                </ul>
            </li>
            <li class="menu-dropdown">
                <a href="#">
                    <i class="menu-icon fa fa-sitemap"></i>
                    {{--<span>Menu levels</span>--}}
                    {{--<span class="fa arrow"></span>--}}
                </a>
                <ul class="sub-menu">
                    <li>
                        <a href="#">
                            <i class="fa fa-fw fa-sitemap"></i> Level 1
                            <span class="fa arrow"></span>
                        </a>
                        <ul class="sub-menu sub-submenu">
                            <li>
                                <a href="#">
                                    <i class="fa fa-fw fa-sitemap"></i> Level 2
                                    <span class="fa arrow"></span>
                                </a>
                                <ul class="sub-menu sub-submenu">
                                    <li>
                                        <a href="#">
                                            <i class="fa fa-fw fa-sitemap"></i> Level 3
                                            <span class="fa arrow"></span>
                                        </a>
                                        <ul class="sub-menu sub-submenu">
                                            <li>
                                                <a href="#">
                                                    <i class="fa fa-fw fa-sitemap"></i> Level 4
                                                    <span class="fa arrow"></span>
                                                </a>
                                                <ul class="sub-menu sub-submenu">
                                                    <li>
                                                        <a href="#">
                                                            <i class="fa fa-fw fa-sitemap"></i> Level 5
                                                        </a>
                                                    </li>
                                                </ul>
                                            </li>
                                        </ul>
                                    </li>
                                </ul>
                            </li>
                        </ul>
                    </li>
                </ul>
            </li>
        </ul>
        {{-- / .navigation --}}
    </div>
    {{-- menu --}}
</section>
{{-- /.sidebar --}}