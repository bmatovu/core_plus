@extends ("layouts.base_min")

@section('extra-css')
    @parent
    {{--page level css--}}
    <link type="text/css" href="{{ asset('css/font-awesome.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('vendors/iCheck/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('vendors/bootstrapvalidator/css/bootstrapValidator.min.css') }}" rel="stylesheet"/>
    <link href="{{ asset('css/login.css') }}" rel="stylesheet">
    {{--end of page level css--}}
@endsection

@push('extra-js')
{{-- begining of page level js --}}
<script type="text/javascript" src="{{ asset('vendors/iCheck/js/icheck.js') }}"></script>
<script src="{{ asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/custom_js/login2.js') }}"></script>
{{-- end of page level js --}}
@endpush

@section('main-content')

    <div class="row">
        <div class="panel-header">
            <h2 class="text-center">
                Log In or
                <a href="{{ url('register2') }}">Sign Up</a>
            </h2>
        </div>
        <div class="panel-body social col-sm-offset-2">
            <div class="col-xs-4 col-sm-3">
                <a href="#" class="btn btn-lg btn-block btn-facebook"> <i class="fa fa-facebook-square fa-lg"></i>
                    <span class="hidden-sm hidden-xs">Facebook</span>
                </a>
            </div>
            <div class="col-xs-4 col-sm-3">
                <a href="#" class="btn btn-lg btn-block btn-twitter"> <i class="fa fa-twitter-square fa-lg"></i>
                    <span class="hidden-sm hidden-xs">Twitter</span>
                </a>
            </div>
            <div class="col-xs-4 col-sm-3">
                <a href="#" class="btn btn-lg btn-block btn-google">
                    <i class="fa fa-google-plus-square fa-lg"></i>
                    <span class="hidden-sm hidden-xs">Google+</span>
                </a>
            </div>
            <div class="clearfix">
                <div class="col-xs-12 col-sm-9">
                    <hr class="omb_hrOr">
                    <span class="omb_spanOr">or</span>
                </div>
                <div class="clearfix"></div>
                <div class="col-xs-12 col-sm-6 form_width">
                    <form action="" id="authentication" method="post" class="login_validator">
                        <div class="form-group">
                            <label for="email" class="sr-only"> E-mail</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-envelope text-primary"></i></span>
                                <input type="text" class="form-control  form-control-lg" id="email" name="username"
                                       placeholder="E-mail">
                            </div>
                        </div>
                        <div class="form-group">
                            <label for="password" class="sr-only">Password</label>

                            <div class="input-group">
                                <span class="input-group-addon"><i class="fa fa-lock text-primary"></i></span>
                                <input type="password" class="form-control form-control-lg" id="password"
                                       name="password" placeholder="Password">
                            </div>
                        </div>
                        <div class="form-group checkbox">
                            <label for="remember">
                                <input type="checkbox" name="remember" id="remember"> Remember Me
                            </label>
                        </div>
                        <div class="form-group">
                            <input type="submit" value="Log In" class="btn btn-primary btn-block"/>
                        </div>
                        <a href="{{ url('forgot_password') }}" id="forgot" class="forgot"> Forgot Password? </a>
                    </form>
                </div>
            </div>
        </div>
    </div>

@endsection