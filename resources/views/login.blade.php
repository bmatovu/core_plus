@extends ("layouts.base_min")

@section('extra-css')
    @parent
    {{--global css--}}
    <link href="{{ asset('css/app.css') }}" rel="stylesheet">
    {{--end of global css--}}
    {{--page level css--}}
    <link rel="stylesheet" href="{{ asset('vendors/iCheck/css/all.css') }}"/>
    <link rel="stylesheet" type="text/css" href="{{ asset('css/custom.css') }}">
    <link href="{{ asset('vendors/iCheck/css/all.css') }}" rel="stylesheet">
    <link href="{{ asset('css/login2.css') }}" rel="stylesheet">
    {{--end page level css--}}
@endsection

@push('extra-js')
{{--global js--}}
<script src="{{ asset('js/backstretch.js') }}"></script>
{{--end of global js--}}
{{--page level js--}}
<script type="text/javascript" src="{{ asset('vendors/iCheck/js/icheck.js') }}"></script>
<script src="{{ asset('vendors/bootstrapvalidator/js/bootstrapValidator.min.js') }}" type="text/javascript"></script>
<script type="text/javascript" src="{{ asset('js/custom_js/login.js') }}"></script>
{{--end of page level js--}}
@endpush

@section('main-content')

    <div class="row " id="form-login">
        <div class="col-md-8 col-md-offset-2 col-sm-8 col-sm-offset-2 col-xs-10 col-xs-offset-1 login-content">
            <div class="row">
                <div class="col-md-12">
                    <div class="header">
                        <h2 class="text-center">
                            Login
                            <small> with</small>
                            <img src="{{ asset('img/pages/logo.png') }}" alt="logo">
                        </h2>
                    </div>
                </div>
            </div>
            <div class="row row-bg-color">
                <div class="col-md-8 core-login">
                    <form class="form-horizontal" method="POST" action="" id="authentication">

                        {{ csrf_field() }}

                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group ">
                                    <label class="control-label" for="email">EMAIL</label>

                                    <div class="input-group">
                                        <input type="text" placeholder="Email Address" class="form-control"
                                               name="username" id="email" value=""/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="row">
                            <div class="col-sm-12">
                                <div class="form-group ">
                                    <label class="control-label" for="password">PASSWORD</label>

                                    <div class="input-group">
                                        <input type="password" placeholder="Password" class="form-control"
                                               name="password" id="password"/>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="form-group">
                            <input type="checkbox" name="remember" id="remember"> &nbsp;
                            <label for="remember"> Remember Me </label>
                            <a href="{{ url('forgot_password') }}" id="forgot" class="text-primary forgot1 pull-right">
                                Forgot Password? </a>
                        </div>
                        <div class="form-group ">
                            <input type="submit" value="Login" class="btn btn-primary login-btn"/>
                            <br>
                            <hr>
                            <span> New to Core Plus?<a href="{{ url('register') }}"> Sign Up</a></span>
                        </div>
                    </form>
                </div>
                <div class="col-md-4">
                    <div class="social-buttons">
                        <p class="text-center">
                            <label>YOU CAN ALSO LOGIN WITH</label>
                        </p>
                        <a class="btn btn-block btn-social btn-google-plus">
                            <i class="fa fa-google-plus"></i> Login with Google
                        </a>
                        <a class="btn btn-block btn-social btn-facebook">
                            <i class="fa fa-facebook"></i> Login with Facebook
                        </a>
                        <a class="btn btn-block btn-social btn-twitter">
                            <i class="fa fa-twitter"></i> Login with Twitter
                        </a>
                        <a class="btn btn-block btn-social btn-linkedin">
                            <i class="fa fa-linkedin"></i> Login with LinkedIn
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection