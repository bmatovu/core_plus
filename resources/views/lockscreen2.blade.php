@extends ("layouts.base_min")

@section('extra-css')
    @parent
    {{--end of global css--}}
    <link href="{{ asset('css/lockscreen.css') }}" rel="stylesheet">
    {{--end page level css--}}
@endsection

@push('extra-js')
{{--page level js--}}
<script src="{{ asset('js/lockscreen.js') }}"></script>
{{--end of page level js--}}
@endpush

@section('pre-main-content')
    <div class="top">
        <div class="colors"></div>
    </div>
@endsection

@section('main-content')

    <div class="lockscreen-container">
        <div id="output"></div>
        <div class="avatar"></div>
        <div class="form-box">
            <form action="#" method="post">
                <div class="form">
                    <p class="form-control-static">ADMIN</p>
                    <input type="password" name="user" class="form-control" placeholder="Password">
                    <button class="btn btn-info btn-block login" id="index" type="submit">GO</button>
                </div>
            </form>
        </div>
    </div>

@endsection