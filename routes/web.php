<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', 'ViewController@show_index');
Route::get('/404', 'ViewController@show_404');
Route::get('/500', 'ViewController@show_500');
Route::get('/addnew_user', 'ViewController@show_addnew_user');
Route::get('/advanced_datatables', 'ViewController@show_advanced_datatables');
Route::get('/advanceddate_pickers', 'ViewController@show_advanceddate_pickers');
Route::get('/advanced_maps', 'ViewController@show_advanced_maps');
Route::get('/advanced_modals', 'ViewController@show_advanced_modals');
Route::get('/amcharts', 'ViewController@show_amcharts');
Route::get('/blank', 'ViewController@show_blank');
Route::get('/bootstrap_tables', 'ViewController@show_bootstrap_tables');
Route::get('/boxed', 'ViewController@show_boxed');
Route::get('/buttons', 'ViewController@show_buttons');
Route::get('/calendar', 'ViewController@show_calendar');
Route::get('/calendar2', 'ViewController@show_calendar2');
Route::get('/chartist', 'ViewController@show_chartist');
Route::get('/chartjs_charts', 'ViewController@show_chartjs_charts');
Route::get('/circle_sliders', 'ViewController@show_circle_sliders');
Route::get('/complex_forms', 'ViewController@show_complex_forms');
Route::get('/complex_forms2', 'ViewController@show_complex_forms2');
Route::get('/datatables', 'ViewController@show_datatables');
Route::get('/datepicker', 'ViewController@show_datepicker');
Route::get('/deleted_users', 'ViewController@show_deleted_users');
Route::get('/dimple_charts', 'ViewController@show_dimple_charts');
Route::get('/draggable_portlets', 'ViewController@show_draggable_portlets');
Route::get('/dropdowns', 'ViewController@show_dropdowns');
Route::get('/dropify', 'ViewController@show_dropify');
Route::get('/edit_user', 'ViewController@show_edit_user');
Route::get('/flot_charts', 'ViewController@show_flot_charts');
Route::get('/fontawesome_icons', 'ViewController@show_fontawesome_icons');
Route::get('/fonts', 'ViewController@show_fonts');
Route::get('/forgot_password', 'ViewController@show_forgot_password');
Route::get('/form_editors', 'ViewController@show_form_editors');
Route::get('/form_elements', 'ViewController@show_form_elements');
Route::get('/form_layouts', 'ViewController@show_form_layouts');
Route::get('/form_validations', 'ViewController@show_form_validations');
Route::get('/form_wizards', 'ViewController@show_form_wizards');
Route::get('/general_components', 'ViewController@show_general_components');
Route::get('/glyphicons', 'ViewController@show_glyphicons');
Route::get('/google_maps', 'ViewController@show_google_maps');
Route::get('/grid_layout', 'ViewController@show_grid_layout');
Route::get('/gridstack', 'ViewController@show_gridstack');
Route::get('/image_filter', 'ViewController@show_image_filter');
Route::get('/image_hover', 'ViewController@show_image_hover');
Route::get('/image_magnifier', 'ViewController@show_image_magnifier');
Route::get('/index', 'ViewController@show_index');
Route::get('/index2', 'ViewController@show_index2');
Route::get('/invoice', 'ViewController@show_invoice');
Route::get('/layout_boxed_fixed_header', 'ViewController@show_layout_boxed_fixed_header');
Route::get('/layout_fixed_header', 'ViewController@show_layout_fixed_header');
Route::get('/layout_fixed', 'ViewController@show_layout_fixed');
Route::get('/layout_horizontal_menu', 'ViewController@show_layout_horizontal_menu');
Route::get('/lockscreen', 'ViewController@show_lockscreen');
Route::get('/lockscreen2', 'ViewController@show_lockscreen2');
Route::get('/login', 'ViewController@show_login');
Route::get('/login2', 'ViewController@show_login2');
Route::get('/masonry_gallery', 'ViewController@show_masonry_gallery');
Route::get('/menubarfold', 'ViewController@show_menubarfold');
Route::get('/multiplefile_upload', 'ViewController@show_multiplefile_upload');
Route::get('/nestable_list', 'ViewController@show_nestable_list');
Route::get('/notifications', 'ViewController@show_notifications');
Route::get('/nvd3_charts', 'ViewController@show_nvd3_charts');
Route::get('/pickers', 'ViewController@show_pickers');
Route::get('/radio_checkboxes', 'ViewController@show_radio_checkboxes');
Route::get('/register', 'ViewController@show_register');
Route::get('/register2', 'ViewController@show_register2');
Route::get('/responsive_datatables', 'ViewController@show_responsive_datatables');
Route::get('/session_timeout', 'ViewController@show_session_timeout');
Route::get('/simple_line_icons', 'ViewController@show_simple_line_icons');
Route::get('/simple_tables', 'ViewController@show_simple_tables');
Route::get('/sweet_alert', 'ViewController@show_sweet_alert');
Route::get('/tabs_accordions', 'ViewController@show_tabs_accordions');
Route::get('/tags_input', 'ViewController@show_tags_input');
Route::get('/timeline', 'ViewController@show_timeline');
Route::get('/toastr_notifications', 'ViewController@show_toastr_notifications');
Route::get('/transitions', 'ViewController@show_transitions');
Route::get('/user_profile', 'ViewController@show_user_profile');
Route::get('/user_list', 'ViewController@show_users');
Route::get('/vector_maps', 'ViewController@show_vector_maps');
Route::get('/x-editable', 'ViewController@show_x_editable');