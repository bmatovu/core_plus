<?php

namespace App\Http\Controllers;

class ViewController extends Controller
{

    public function show_404()
    {
        return view('errors.404', ['title' => 'Page Not Found :: Core+']);
    }

    public function show_500()
    {
        return view('errors.500', ['title' => 'Internal Server Error :: Core+']);
    }

    public function show_addnew_user()
    {
        return view('addnew_user', ['title' => 'Add New User :: Core+', 'select' => ['users', 'addnew_user'],]);
    }

    public function show_advanced_datatables()
    {
        return view('advanced_datatables', ['title' => 'Advanced Datatables :: Core+', 'select' => ['tables', 'advanced_datatables'],]);
    }

    public function show_advanceddate_pickers()
    {
        return view('advanceddate_pickers', ['title' => 'Advanced Date Pickers :: Core+', 'select' => ['forms', 'advanceddate_pickers'],]);
    }

    public function show_advanced_maps()
    {
        return view('advanced_maps', ['title' => 'Advanced Maps :: Core+', 'select' => ['maps', 'advanced_maps'],]);
    }

    public function show_advanced_modals()
    {
        return view('advanced_modals', ['title' => 'Advanced Modals :: Core+', 'select' => ['ui-features', 'advanced_modals'],]);
    }

    public function show_amcharts()
    {
        return view('amcharts', ['title' => 'Amcharts :: Core+', 'select' => ['charts', 'amcharts'],]);
    }

    public function show_blank()
    {
        return view('blank', ['title' => 'Blank :: Core+', 'select' => ['pages', 'blank'],]);
    }

    public function show_bootstrap_tables()
    {
        return view('bootstrap_tables', ['title' => 'Bootstrap Tables :: Core+', 'select' => ['tables', 'bootstrap_tables'],]);
    }

    public function show_boxed()
    {
        return view('boxed', ['title' => 'Boxed :: Core+', 'select' => ['layouts', 'boxed'],]);
    }

    public function show_buttons()
    {
        return view('buttons', ['title' => 'Buttons :: Core+', 'select' => ['ui-features', 'buttons'],]);
    }

    public function show_calendar()
    {
        return view('calendar', ['title' => 'Calendar :: Core+', 'select' => ['calendars', 'calendar'],]);
    }

    public function show_calendar2()
    {
        return view('calendar2', ['title' => 'Calendar#2 :: Core+', 'select' => ['calendars', 'calendar2'],]);
    }

    public function show_chartist()
    {
        return view('chartist', ['title' => 'Chartist :: Core+', 'select' => ['charts', 'chartist'],]);
    }

    public function show_chartjs_charts()
    {
        return view('chartjs_charts', ['title' => 'Chartjs Charts :: Core+', 'select' => ['charts', 'chartjs_charts'],]);
    }

    public function show_circle_sliders()
    {
        return view('circle_sliders', ['title' => 'Circle Sliders :: Core+', 'select' => ['charts', 'circle_sliders'],]);
    }

    public function show_complex_forms()
    {
        return view('complex_forms', ['title' => 'Complex Forms :: Core+', 'select' => ['forms', 'complex_forms'],]);
    }

    public function show_complex_forms2()
    {
        return view('complex_forms2', ['title' => 'Complex Forms#2 :: Core+', 'select' => ['forms', 'complex_forms2'],]);
    }

    public function show_datatables()
    {
        return view('datatables', ['title' => 'Datatables :: Core+', 'select' => ['tables', 'datatables'],]);
    }

    public function show_datepicker()
    {
        return view('datepicker', ['title' => 'Datepicker :: Core+', 'select' => ['forms', 'datepicker'],]);
    }

    public function show_deleted_users()
    {
        return view('deleted_users', ['title' => 'Deleted Users :: Core+', 'select' => ['users', 'deleted_users'],]);
    }

    public function show_dimple_charts()
    {
        return view('dimple_charts', ['title' => 'Dimple Charts :: Core+', 'select' => ['charts', 'dimple_charts'],]);
    }

    public function show_draggable_portlets()
    {
        return view('draggable_portlets', ['title' => 'Draggable Portlets :: Core+', 'select' => ['ui-features',
            'draggable_portlets'],]);
    }

    public function show_dropdowns()
    {
        return view('dropdowns', ['title' => 'Dropdowns :: Core+', 'select' => ['forms', 'dropdowns'],]);
    }

    public function show_dropify()
    {
        return view('dropify', ['title' => 'Dropify :: Core+', 'select' => ['gallery', 'dropify'],]);
    }

    public function show_edit_user()
    {
        return view('edit_user', ['title' => 'Edit User :: Core+', 'select' => ['users', 'edit_user'],]);
    }

    public function show_flot_charts()
    {
        return view('flot_charts', ['title' => 'Flot Charts :: Core+', 'select' => ['charts', 'flot_charts'],]);
    }

    public function show_fontawesome_icons()
    {
        return view('fontawesome_icons', ['title' => 'Font Awesome Icons :: Core+', 'select' => ['ui-features', 'fonts', 'fontawesome_icons'],]);
    }

    public function show_fonts()
    {
        return view('fonts', ['title' => 'Fonts :: Core+', 'select' => ['ui-features', 'fonts'],]);
    }

    public function show_forgot_password()
    {
        return view('forgot_password', ['title' => 'Forgot Password :: Core+',]);
    }

    public function show_form_editors()
    {
        return view('form_editors', ['title' => 'Form Editors :: Core+', 'select' => ['forms', 'form_editors'],]);
    }

    public function show_form_elements()
    {
        return view('form_elements', ['title' => 'Form Elements :: Core+', 'select' => ['forms', 'form_elements'],]);
    }

    public function show_form_layouts()
    {
        return view('form_layouts', ['title' => 'Form Layouts :: Core+', 'select' => ['forms', 'form_layouts'],]);
    }

    public function show_form_validations()
    {
        return view('form_validations', ['title' => 'Form Validations :: Core+', 'select' => ['forms', 'form_validations'],]);
    }

    public function show_form_wizards()
    {
        return view('form_wizards', ['title' => 'Form Wizards :: Core+', 'select' => ['forms', 'form_wizards'],]);
    }

    public function show_general_components()
    {
        return view('general_components', ['title' => 'General Components :: Core+', 'select' => ['ui-features', 'general_components'],]);
    }

    public function show_glyphicons()
    {
        return view('glyphicons', ['title' => 'Glyphicons :: Core+', 'select' => ['ui-features', 'fonts', 'glyphicons'],]);
    }

    public function show_google_maps()
    {
        return view('google_maps', ['title' => 'Google Maps :: Core+', 'select' => ['maps', 'google_maps'],]);
    }

    public function show_grid_layout()
    {
        return view('grid_layout', ['title' => 'Grid Layout :: Core+', 'select' => ['ui-features', 'grid_layout'],]);
    }

    public function show_gridstack()
    {
        return view('gridstack', ['title' => 'Grid Stack :: Core+', 'select' => ['ui-features', 'gridstack'],]);
    }

    public function show_image_filter()
    {
        return view('image_filter', ['title' => 'Image Filter :: Core+', 'select' => ['gallery', 'image_filter'],]);
    }

    public function show_image_hover()
    {
        return view('image_hover', ['title' => 'Image Hover :: Core+', 'select' => ['gallery', 'image_hover'],]);
    }

    public function show_image_magnifier()
    {
        return view('image_magnifier', ['title' => 'Image Magnifier :: Core+', 'select' => ['gallery', 'image_magnifier'],]);
    }

    public function show_index()
    {
        return view('index', ['title' => 'Core+ :: Admin Template', 'select' => ['index'],]);
    }

    public function show_index2()
    {
        return view('index2', ['title' => 'Core+ :: Admin Template', 'select' => ['index2'],]);
    }

    public function show_invoice()
    {
        return view('invoice', ['title' => 'Invoice :: Core+', 'select' => ['pages', 'invoice'],]);
    }

    public function show_layout_boxed_fixed_header()
    {
        return view('layout_boxed_fixed_header', ['title' => 'Layout Boxed Fixed Header :: Core+', 'select' => ['layouts',
            'layout_boxed_fixed_header'],]);
    }

    public function show_layout_fixed_header()
    {
        return view('layout_fixed_header', ['title' => 'Layout Fixed Header :: Core+', 'select' => ['layouts', 'layout_fixed_header'],]);
    }

    public function show_layout_fixed()
    {
        return view('layout_fixed', ['title' => 'Layout Fixed :: Core+', 'select' => ['layouts', 'layout_fixed'],]);
    }

    public function show_layout_horizontal_menu()
    {
        return view('layout_horizontal_menu', ['title' => 'Layout Horizontal Menu :: Core+', 'select' => ['layouts',
            'layout_horizontal_menu'],]);
    }

    public function show_lockscreen()
    {
        return view('lockscreen', ['title' => 'Lockscreen :: Core+', 'body_cls' => 'background-img', 'select' => ['pages', 'lockscreen'],]);
    }

    public function show_lockscreen2()
    {
        return view('lockscreen2', ['title' => 'Lockscreen#2 :: Core+', 'select' => ['pages', 'lockscreen2'],]);
    }

    public function show_login()
    {
        return view('login', ['title' => 'Admin Login :: Core+', 'body_cls' => 'bg-slider', 'select' => ['pages', 'login'],]);
    }

    public function show_login2()
    {
        return view('login2', ['title' => 'Login#2 :: Core+', 'select' => ['pages', 'login2'],]);
    }

    public function show_masonry_gallery()
    {
        return view('masonry_gallery', ['title' => 'Masonry Gallery :: Core+', 'select' => ['gallery', 'masonry_gallery'],]);
    }

    public function show_menubarfold()
    {
        return view('menubarfold', ['title' => 'Menu Bar Fold :: Core+', 'select' => ['layouts', 'menubarfold'],]);
    }

    public function show_multiplefile_upload()
    {
        return view('multiplefile_upload', ['title' => 'Multiplefile Upload :: Core+', 'select' => ['gallery', 'multiplefile_upload'],]);
    }

    public function show_nestable_list()
    {
        return view('nestable_list', ['title' => 'Nestable List :: Core+', 'select' => ['ui-features', 'nestable_list'],]);
    }

    public function show_notifications()
    {
        return view('notifications', ['title' => 'Notifications :: Core+', 'select' => ['ui-features', 'notifications'],]);
    }

    public function show_nvd3_charts()
    {
        return view('nvd3_charts', ['title' => 'NVD3 Charts :: Core+', 'select' => ['charts', 'nvd3_charts'],]);
    }

    public function show_pickers()
    {
        return view('pickers', ['title' => 'Pickers :: Core+', 'select' => ['ui-features', 'pickers'],]);
    }

    public function show_radio_checkboxes()
    {
        return view('radio_checkboxes', ['title' => 'Radio Checkboxes :: Core+', 'select' => ['forms', 'radio_checkboxes'],]);
    }

    public function show_register()
    {
        return view('register', ['title' => 'Register :: Core+', 'select' => ['pages', 'register'],]);
    }

    public function show_register2()
    {
        return view('register2', ['title' => 'Register#2 :: Core+', 'select' => ['pages', 'register'],]);
    }

    public function show_responsive_datatables()
    {
        return view('responsive_datatables', ['title' => 'Responsive Datatables :: Core+', 'select' => ['tables', 'responsive_datatables'],]);
    }

    public function show_session_timeout()
    {
        return view('session_timeout', ['title' => 'Session Timeout :: Core+', 'select' => ['ui-features', 'session_timeout'],]);
    }

    public function show_simple_line_icons()
    {
        return view('simple_line_icons', ['title' => 'Simple Line Icons :: Core+', 'select' => ['ui-features', 'fonts', 'simple_line_icons'],]);
    }

    public function show_simple_tables()
    {
        return view('simple_tables', ['title' => 'Simple Tables :: Core+', 'select' => ['tables', 'simple_tables'],]);
    }

    public function show_sweet_alert()
    {
        return view('sweet_alert', ['title' => 'Sweet Alert :: Core+', 'select' => ['ui-features', 'sweet_alert'],]);
    }

    public function show_tabs_accordions()
    {
        return view('tabs_accordions', ['title' => 'Tabs Accordions :: Core+', 'select' => ['ui-features', 'tabs_accordions'],]);
    }

    public function show_tags_input()
    {
        return view('tags_input', ['title' => 'Tags Input :: Core+', 'select' => ['ui-features', 'tags_input'],]);
    }

    public function show_timeline()
    {
        return view('timeline', ['title' => 'Timeline :: Core+', 'select' => ['ui-components', 'timeline'],]);
    }

    public function show_toastr_notifications()
    {
        return view('toastr_notifications', ['title' => 'Toastr Notifications :: Core+', 'select' => ['ui-features', 'toastr_notifications'],]);
    }

    public function show_transitions()
    {
        return view('transitions', ['title' => 'Transitions :: Core+', 'select' => ['ui-components', 'transitions'],]);
    }

    public function show_user_profile()
    {
        return view('user_profile', ['title' => 'User Profile :: Core+', 'select' => ['user', 'user_profile'],]);
    }

    public function show_users()
    {
        return view('users', ['title' => 'Users :: Core+', 'select' => ['users', 'user_list'],]);
    }

    public function show_vector_maps()
    {
        return view('vector_maps', ['title' => 'Vector Maps :: Core+', 'select' => ['maps', 'vector_maps'],]);
    }

    public function show_x_editable()
    {
        return view('x-editable', ['title' => 'x-editable :: Core+', 'select' => ['forms', 'x-editable'],]);
    }

}